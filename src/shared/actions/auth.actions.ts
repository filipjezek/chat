import { createAction, props } from '@ngrx/store';

export const submitNick = createAction('[Login Page] Submit nickname', props<{ nick: string }>());
export const sessionCreated = createAction('[Auth API] Session created', props<{ session: string }>());
export const loadSession = createAction('[Chat] Load session');
export const sessionInvalid = createAction('[Auth API] Session invalid');
export const sessionRestored = createAction('[Auth API] Session restored', props<{ nick: string }>());
