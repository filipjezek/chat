import { createAction, props } from '@ngrx/store';
import { EncryptedMessage, Notification, MsgStub, DecryptedMessage } from '../interfaces/message';
import { UserStatus } from '../interfaces/user-status';
import { Member } from '../interfaces/member';

export const createRoom = createAction('[Select Page] Create room', props<{ roomID: string }>());
export const joinRoom = createAction('[Select Page] Join room', props<{ roomID: string }>());
export const roomCreated = createAction('[Room API] Room created', props<{ nick: string; roomID: string }>());
export const roomJoined = createAction(
  '[Room API] Room joined',
  props<{
    members: Member[];
    messages: (DecryptedMessage | Notification)[];
    nick: string;
    passphrase: string;
    isAdmin: boolean;
  }>()
);
export const roomJoinedRawData = createAction(
  '[Room API] Room joined raw data',
  props<{
    members: {
      nick: string;
      status: UserStatus;
      privateMessages: (EncryptedMessage | MsgStub)[];
      isAdmin: boolean;
    }[];
    messages: (EncryptedMessage | Notification | MsgStub)[];
    passphrase: string;
    nick: string;
    isAdmin: boolean;
  }>()
);
export const cancelJoin = createAction('[Select Page] Cancel join');
export const passphraseRequired = createAction('[Room API] Passphrase required');
export const sendPassphrase = createAction('[Select Page] Send passphrase', props<{ passphrase: string }>());
export const apiError = createAction('[Room API] API error', props<{ error: any }>());
export const selectPageErrorSeen = createAction('[Select Page] Error seen');
export const roomPageErrorSeen = createAction('[Room Page] Error seen');
export const leaveRoom = createAction('[Room Page] Leave room');
export const lockRoom = createAction('[Room Page] Lock room', props<{ passphrase: string }>());
export const roomLocked = createAction('[Room API] Room locked', props<{ passphrase: string }>());
export const sendMessage = createAction('[Room Page] Send message', props<{ message: string }>());
export const messageSent = createAction('[Room Page] Message sent', props<{ message: DecryptedMessage }>());
export const messageReceived = createAction(
  '[Room Page] Message received',
  props<{ message: DecryptedMessage }>()
);
export const messageReceivedEncrypted = createAction(
  '[Room API] Message received encrypted',
  props<{ message: EncryptedMessage }>()
);
export const notificationReceived = createAction(
  '[Room API] Notification received',
  props<{ message: Notification }>()
);
export const openPrivateConversation = createAction(
  '[Room Page] Open private conversation',
  props<{ member: string }>()
);
export const closePrivateConversation = createAction('[Room Page] Close private conversation');
export const whisper = createAction('[Room Page] Whisper', props<{ message: string; to: string }>());
export const whispered = createAction(
  '[Room Page] Whispered',
  props<{ message: DecryptedMessage; to: string }>()
);
export const whisperReceived = createAction(
  '[Room Page] Whisper received',
  props<{ message: DecryptedMessage }>()
);
export const whisperReceivedEncrypted = createAction(
  '[Room API] Whisper received encrypted',
  props<{ message: EncryptedMessage }>()
);
export const readPrivateMessages = createAction(
  '[Room Page] Read private messages',
  props<{ member: string }>()
);
export const kickoutMember = createAction('[Room Page] Kickout member', props<{ member: string }>());
export const memberKickedOut = createAction('[Room API] Member kicked out', props<{ member: string }>());
export const meKickedOut = createAction('[Room API] Me kicked out');
export const memberJoined = createAction('[Room API] Member joined', props<{ member: string }>());
export const memberLeft = createAction('[Room API] Member left', props<{ member: string }>());
export const memberDeleted = createAction('[Room API] Member deleted', props<{ member: string }>());
