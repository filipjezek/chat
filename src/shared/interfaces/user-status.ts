export enum UserStatus {
  Online = 'online',
  Offline = 'offline',
  Deleted = 'deleted',
  KickedOut = 'kicked-out'
}
