import { UserStatus } from './user-status';
import { DecryptedMessage } from './message';

export interface Member {
  nick: string;
  status: UserStatus;
  color: string;
  unreadMessages: number;
  privateMessages: DecryptedMessage[];
}
