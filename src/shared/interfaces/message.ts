/**
 * Stored on BE
 */
export class InternalMessage {
  public date: Date;

  static fromSerialized(m: SerializedInternalMessage, author: string) {
    const im = new this(m.ciphertext, new Map(m.keys), m.iv, author, m.isPrivate);
    im.date = m.date;
    return im;
  }

  constructor(
    public ciphertext: ArrayBuffer,
    public keys: Map<string, { body: ArrayBuffer; type: number }>,
    public iv: ArrayBuffer,
    public author: string,
    public isPrivate = false
  ) {
    this.date = new Date();
  }
}

/**
 * Sent from FE to BE.
 * No author information included
 */
export class SerializedInternalMessage {
  public date: Date;

  constructor(
    public ciphertext: ArrayBuffer,
    public keys: [string, { body: ArrayBuffer; type: number }][],
    public iv: ArrayBuffer,
    public isPrivate = false
  ) {
    this.date = new Date();
  }
}

/**
 * Sent from BE to FE.
 * Only one key (for the specific person)
 */
export class EncryptedMessage {
  constructor(
    public ciphertext: ArrayBuffer,
    public key: { body: ArrayBuffer; type: number },
    public iv: ArrayBuffer,
    public author: string,
    public date: Date
  ) {}
}

/**
 * Is not encrypted at all
 */
export class Notification {
  public date: Date;

  constructor(public text: string) {
    this.date = new Date();
  }
}

/**
 * Placeholder for the messages sent by the user himself.
 * Those are stored locally in plaintext
 */
export class MsgStub {
  constructor(public date: Date) {}
}

/**
 * Decrypted message representation on FE
 */
export interface DecryptedMessage {
  text: string;
  author: string;
  date: Date;
}
