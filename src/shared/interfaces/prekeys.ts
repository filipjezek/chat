export interface preKey {
  keyId: string;
  publicKey: ArrayBuffer;
}
export interface preKeyBundle {
  registrationId: number;
  identityKey: ArrayBuffer;
  signedPreKey: preKey & { signature: ArrayBuffer };
  preKeys: preKey[];
}
