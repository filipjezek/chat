export enum SocketMessages {
  NewUser = 'new user',
  ExistingUser = 'existing user',
  CreateRoom = 'create room',
  JoinRoom = 'join room',
  Passphrase = 'passphrase',
  LockRoom = 'set passphrase',
  LeaveRoom = 'leave room',
  KickoutMember = 'kickout member',
  SendMessage = 'send message'
}
export enum SignalSocketMessages {
  PublishNewPrekeys = 'publish new prekeys',
  NewPrekeysRequired = 'new prekeys required',
  RequestBundles = 'request bundles',
  Bundles = 'bundles'
}
