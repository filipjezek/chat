import { Group } from '../group';
import { apiError, roomCreated } from '../../shared/actions/room.actions';
import { User } from '../user';
import * as crypto from 'crypto';

export function createGroup(groupList: Record<string, Group>, id: string, user: User) {
  if (!id || !id.match(/^[a-z0-9+-]{8,}$/i)) {
    do {
      id = crypto
        .randomBytes(11)
        .toString('base64')
        .substr(0, 10)
        .replace(/\//g, '-')
        .toLowerCase();
    } while (id in groupList);
  } else {
    id = id.toLowerCase();
    if (id in groupList) {
      user.socket.emit('ngrx action', apiError({ error: 'Room already exists' }));
      return;
    }
  }

  groupList[id] = new Group(user);
  groupList[id].onDeleted = () => delete groupList[id];
  user.currentGroup = groupList[id];

  user.socket.emit('ngrx action', roomCreated({ nick: user.baseName, roomID: id }));
  groupList[id].notify(`${user.baseName} created the room.`);
}
