import { User } from '../user';
import { UserStatus } from '../../shared/interfaces/user-status';
import * as crypto from 'crypto';
import { memberKickedOut, roomLocked } from '../../shared/actions/room.actions';

export function kickoutMember(user: User, toKickoutNick: string) {
  const group = user.currentGroup;
  if (!group || user !== group.admin) {
    return;
  }

  const toKickout = group.members[toKickoutNick].user;

  group.notify(`${toKickoutNick} was kicked out.`, memberKickedOut({ member: toKickoutNick }));

  group.setUserStatus(toKickout, UserStatus.KickedOut);
  toKickout.currentGroup = null;
  const passphrase = crypto
    .randomBytes(20)
    .toString('base64')
    .substr(0, 24);
  group.setPassphrase(passphrase);
  group.notify('Passphrase was changed automatically.', roomLocked({ passphrase }));
}
