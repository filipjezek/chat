import { User } from '../user';
import { SignalSocketMessages } from '../../shared/interfaces/socket-messages';

export function getBundles(user: User, nicks: string[]) {
  if (!user || !user.currentGroup) {
    return;
  }

  const g = user.currentGroup;
  user.socket.emit(
    SignalSocketMessages.Bundles,
    nicks.map(nick => {
      const bundle = g.members[nick] && g.members[nick].user && g.members[nick].user.preKeyBundle;
      if (!bundle) {
        return null;
      }

      const oneTimePrekey = bundle.preKeys.pop();
      if (bundle.preKeys.length < 5) {
        g.members[nick].user.promptForPrekeys();
      }
      return [
        nick,
        {
          preKey: oneTimePrekey,
          signedPreKey: bundle.signedPreKey,
          registrationId: bundle.registrationId,
          identityKey: bundle.identityKey
        }
      ];
    })
  );
}
