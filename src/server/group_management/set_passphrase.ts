import { apiError, roomLocked } from '../../shared/actions/room.actions';
import { User } from '../user';
import { UserStatus } from '../../shared/interfaces/user-status';

export function setPassphrase(user: User, passphrase: string) {
  if (typeof passphrase !== 'string') {
    user.socket.emit('ngrx action', apiError({ error: 'Passphrase must be string' }));
  }
  if (!user.currentGroup || user.currentGroup.getUserStatus(user) !== UserStatus.Online) {
    user.socket.emit('ngrx action', apiError({ error: 'User is not in the room' }));
    return;
  }

  user.currentGroup.setPassphrase(passphrase);
  user.currentGroup.notify(
    `${user.currentGroup.nickNames.get(user)} changed the lock.`,
    roomLocked({ passphrase })
  );
}
