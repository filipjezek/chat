import { User } from '../user';
import { UserStatus } from '../../shared/interfaces/user-status';
import { memberLeft } from '../../shared/actions/room.actions';

export function leaveGroup(user: User) {
  if (!user.currentGroup) {
    return;
  }

  user.currentGroup.setUserStatus(user, UserStatus.Offline);

  const nick = user.currentGroup.nickNames.get(user);
  user.currentGroup.notify(`${nick} left the chat.`, memberLeft({ member: nick }));
  user.currentGroup = null;
}
