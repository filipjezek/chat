import {
  SerializedInternalMessage,
  InternalMessage,
  EncryptedMessage
} from '../../shared/interfaces/message';
import { User } from '../user';
import { UserStatus } from '../../shared/interfaces/user-status';
import { messageReceivedEncrypted, whisperReceivedEncrypted } from '../../shared/actions/room.actions';

export function sendMessage(user: User, message: SerializedInternalMessage) {
  const g = user.currentGroup;
  const internal = InternalMessage.fromSerialized(message, g.nickNames.get(user));
  g.messages.add(internal);

  internal.keys.forEach((key, nick) => {
    if (g.members[nick] && g.members[nick].status === UserStatus.Online) {
      g.members[nick].user.socket.emit(
        'ngrx action',
        (internal.isPrivate ? whisperReceivedEncrypted : messageReceivedEncrypted)({
          message: new EncryptedMessage(internal.ciphertext, key, internal.iv, internal.author, internal.date)
        })
      );
    }
  });
}
