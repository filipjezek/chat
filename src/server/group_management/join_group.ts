import { Group } from '../group';
import {
  passphraseRequired,
  apiError,
  roomJoinedRawData,
  memberJoined
} from '../../shared/actions/room.actions';
import { User } from '../user';
import { UserStatus } from '../../shared/interfaces/user-status';

export function joinGroup(group: Group, user: User, passphrase = '') {
  if (!group) {
    user.socket.emit('ngrx action', apiError({ error: 'Room does not exist' }));
    return;
  }
  const status = group.getUserStatus(user);

  if (status !== UserStatus.Offline && !group.validatePassphrase(passphrase)) {
    user.socket.emit('ngrx action', passphraseRequired());
    return;
  }

  const nick = group.addUser(user);
  user.currentGroup = group;

  group.notify(`${nick} joined the chat.`, memberJoined({ member: nick }));

  user.socket.emit(
    'ngrx action',
    roomJoinedRawData({
      nick,
      ...group.formatData(nick)
    })
  );
}
