import { User } from '../user';
import { Socket } from 'socket.io';
import * as uuid from 'uuid/v4';
import { sessionCreated } from '../../shared/actions/auth.actions';
import { preKeyBundle } from '../../shared/interfaces/prekeys';

export function createUser(
  baseName: string,
  memberList: { [key: string]: User },
  socket: Socket,
  bundle: preKeyBundle
): { sessionId: string; user: User } {
  if (!baseName || !baseName.match(/^[^\r\n\t\f\v ()#.]{3,}$/)) {
    return { user: null, sessionId: null };
  }

  let user = new User(baseName, socket, bundle),
    sessionId: string;
  do {
    sessionId = uuid();
  } while (sessionId in memberList);
  memberList[sessionId] = user;
  user.onDeleted = () => delete memberList[sessionId];

  socket.emit('ngrx action', sessionCreated({ session: sessionId }));
  return { sessionId, user };
}
