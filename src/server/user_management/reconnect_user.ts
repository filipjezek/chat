import { User } from '../user';
import { Socket } from 'socket.io';
import { sessionRestored, sessionInvalid } from '../../shared/actions/auth.actions';

export function reconnectUser(session: string, memberList: { [key: string]: User }, socket: Socket): User {
  let user = memberList[session];
  if (!user) {
    socket.emit('ngrx action', sessionInvalid());
  } else {
    user.setSocket(socket);
    socket.emit('ngrx action', sessionRestored({ nick: user.baseName }));
  }

  return user;
}
