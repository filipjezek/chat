import * as express from 'express';
import * as IO from 'socket.io';
import * as helmet from 'helmet';
import * as https from 'https';
import * as fs from 'fs';
import * as path from 'path';
import { User } from './user';
import { createUser } from './user_management/create-user';
import { reconnectUser } from './user_management/reconnect_user';
import { createGroup } from './group_management/create_group';
import { joinGroup } from './group_management/join_group';
import { setPassphrase } from './group_management/set_passphrase';
import { leaveGroup } from './group_management/leave_group';
import { kickoutMember } from './group_management/kickout_member';
import { getBundles } from './group_management/get_bundles';
import { sendMessage } from './group_management/send_message';
import { SocketMessages, SignalSocketMessages } from '../shared/interfaces/socket-messages';

const app = express();
const port = 3000;
const options = {
  key: fs.readFileSync(path.resolve(__dirname, '../certs/server.key')),
  cert: fs.readFileSync(path.resolve(__dirname, '../certs/server.crt')),
  dhparam: fs.readFileSync(path.resolve(__dirname, '../certs/dh-strong.pem')),
};

app.use(helmet());

app.get(
  '*.*',
  express.static(path.resolve(__dirname, '../chat/'), {
    maxAge: '1y',
  })
);
app.get('*', (req, res) => {
  fs.readFile(path.resolve(__dirname, '../chat/index.html'), (e, data) => {
    if (e) {
      console.error(e);
      res.status(500).end(e.message);
    } else {
      res.type('html').send(data).end();
    }
  });
});

const server = https
  .createServer(options, app)
  .listen(port, () => console.log(`Babylon server listening on port ${port}!`));
const io = IO(server, { serveClient: false });

const groups = {};
const members = {};

io.on('connection', (socket) => {
  let user: User, sessionId: string;

  socket.on(SocketMessages.NewUser, (baseName, bundle) => {
    if (user) {
      return;
    }
    ({ user, sessionId } = createUser(baseName, members, socket, bundle));
  });
  socket.on(SocketMessages.ExistingUser, (session) => {
    user = reconnectUser(session, members, socket);
  });
  socket.on('reconnect', () => {
    user = reconnectUser(sessionId, members, socket);
  });

  socket.on(SocketMessages.JoinRoom, (id, passphrase) => {
    if (user) {
      joinGroup(groups[id], user, passphrase);
    }
  });
  socket.on(SocketMessages.LeaveRoom, () => {
    if (user) {
      leaveGroup(user);
    }
  });
  socket.on(SocketMessages.CreateRoom, (id) => {
    if (user) {
      createGroup(groups, id, user);
    }
  });
  socket.on(SocketMessages.LockRoom, (passphrase) => {
    if (user) {
      setPassphrase(user, passphrase);
    }
  });
  socket.on(SocketMessages.KickoutMember, (who) => {
    if (user) {
      kickoutMember(user, who);
    }
  });
  socket.on(SignalSocketMessages.RequestBundles, (nicks) => {
    getBundles(user, nicks);
  });
  socket.on(SignalSocketMessages.PublishNewPrekeys, (prekeys) => {
    if (user) {
      user.preKeyBundle.preKeys.concat(prekeys.filter((x) => !!x));
    }
  });
  socket.on(SocketMessages.SendMessage, (msg) => {
    if (user && user.currentGroup) {
      sendMessage(user, msg);
    }
  });
});
