import { Messages } from './messages';
import { User } from './user';
import { EncryptedMessage, Notification, MsgStub } from '../shared/interfaces/message';
import { UserStatus } from '../shared/interfaces/user-status';
import { notificationReceived } from '../shared/actions/room.actions';
import { Action } from '@ngrx/store';

type Members = {
  [nick: string]: {
    user: User;
    status: UserStatus;
  };
};

export class Group {
  public members: Members = {};
  public nickNames = new WeakMap<User, string>();
  public messages = new Messages();
  public deleteTimeout: NodeJS.Timer;
  public onDeleted: () => void;
  private passphrase: string = '';

  constructor(public readonly admin: User) {
    this.addUser(admin);
  }

  public addUser(user: User): string {
    let groupNick = user.baseName;
    for (let i = 2; this.members[groupNick] && this.members[groupNick].user !== user; i++) {
      groupNick = `${user.baseName}(${i})`;
    }
    this.members[groupNick] = {
      user,
      status: UserStatus.Online
    };
    this.nickNames.set(user, groupNick);
    clearTimeout(this.deleteTimeout);
    return groupNick;
  }
  public setUserStatus(user: User, status: UserStatus) {
    const groupNick = this.nickNames.get(user);
    if (!groupNick) {
      throw new Error('user does not exist');
    }

    this.members[groupNick].status = status;
    if (status === UserStatus.Deleted) {
      this.members[groupNick].user = null;
      this.nickNames.delete(user);
    } else if (status === UserStatus.Offline) {
      let empty = true;
      for (const m in this.members) {
        if (this.members[m] && this.members[m].status) {
          empty = false;
          break;
        }
      }
      if (empty) {
        this.deleteTimeout = setTimeout(() => this.onDeleted(), 20 * 60 * 1000);
      }
    } else {
      clearTimeout(this.deleteTimeout);
    }
  }
  public getUserStatus(user: User): UserStatus {
    return (this.members[this.nickNames.get(user)] || ({} as any)).status;
  }

  public formatData(forUser: string) {
    const members: {
      nick: string;
      status: UserStatus;
      privateMessages: (EncryptedMessage | MsgStub)[];
      isAdmin: boolean;
    }[] = [];
    const messages = this.messages.getForUser(forUser);

    for (const nick in this.members) {
      if (nick !== forUser) {
        const usr = this.members[nick].user;
        members.push({
          nick,
          status: this.members[nick].status,
          privateMessages: messages.priv[nick] || [],
          isAdmin: usr === this.admin
        });
      }
    }

    return {
      members,
      messages: messages.pub,
      passphrase: this.passphrase,
      isAdmin: this.admin === this.members[forUser].user
    };
  }

  public validatePassphrase(pass: string) {
    return pass === this.passphrase;
  }

  public setPassphrase(pass: string) {
    this.passphrase = pass;
  }

  public notify(notification?: string, action?: Action) {
    const n = new Notification(notification);
    this.messages.add(n);

    for (const m in this.members) {
      const u = this.members[m];
      if (u.status === UserStatus.Online) {
        if (notification) {
          u.user.socket.emit('ngrx action', notificationReceived({ message: n }));
        }
        if (action) {
          u.user.socket.emit('ngrx action', action);
        }
      }
    }
  }
}
