const path = require('path');

module.exports = {
  entry: './server.ts',
  target: 'node',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, '../../dist/server'),
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
        sideEffects: false,
      },
    ],
  },
  node: {
    __dirname: false,
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  mode: 'production',
};
