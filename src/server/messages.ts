import { EncryptedMessage, InternalMessage, Notification, MsgStub } from '../shared/interfaces/message';

export class Messages {
  private list: (InternalMessage | Notification)[] = [];

  public add(m: InternalMessage | Notification) {
    this.list.push(m);
  }

  public getForUser(
    name: string
  ): {
    pub: (EncryptedMessage | Notification | MsgStub)[];
    priv: Record<string, (EncryptedMessage | MsgStub)[]>;
  } {
    const pub = [];
    const priv: Record<string, (EncryptedMessage | MsgStub)[]> = {};
    for (const msg of this.list) {
      if (msg instanceof Notification) {
        pub.push(msg);
      } else if (!msg.isPrivate && msg.keys.has(name)) {
        pub.push(new EncryptedMessage(msg.ciphertext, msg.keys.get(name), msg.iv, msg.author, msg.date));
      } else if (!msg.isPrivate && msg.author === name) {
        pub.push(new MsgStub(msg.date));
      } else if (msg.keys.has(name)) {
        const otherParty: string = msg.author;
        if (!priv[otherParty]) {
          priv[otherParty] = [];
        }
        priv[otherParty].push(
          new EncryptedMessage(msg.ciphertext, msg.keys.get(name), msg.iv, msg.author, msg.date)
        );
      } else if (msg.author === name) {
        const otherParty: string = msg.keys.keys().next().value;
        if (!priv[otherParty]) {
          priv[otherParty] = [];
        }
        priv[otherParty].push(new MsgStub(msg.date));
      }
    }
    return { pub, priv };
  }
}
