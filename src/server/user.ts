import { Socket } from 'socket.io';
import { Group } from './group';
import { joinGroup } from './group_management/join_group';
import { UserStatus } from '../shared/interfaces/user-status';
import { memberLeft, memberDeleted } from '../shared/actions/room.actions';
import { preKeyBundle } from '../shared/interfaces/prekeys';
import { SignalSocketMessages } from '../shared/interfaces/socket-messages';

export class User {
  public socket: Socket;
  public onDeleted: () => void;
  public set currentGroup(g: Group) {
    this._currentGroup = g;
    if (g) {
      this.groupsVisited.add(g);
    }
  }
  public get currentGroup(): Group {
    return this._currentGroup;
  }

  private deleteTimeout: NodeJS.Timer;
  private groupsVisited = new Set<Group>();
  private preKeysNeeded = false;

  private _currentGroup: Group;

  constructor(public baseName: string, socket: Socket, public preKeyBundle: preKeyBundle) {
    this.setSocket(socket);
  }

  public setSocket(socket: Socket) {
    clearTimeout(this.deleteTimeout);
    if (socket === this.socket) {
      if (this.currentGroup) {
        joinGroup(this.currentGroup, this);
      }
    } else {
      this.socket = socket;
      this.currentGroup = null;
      socket.on('disconnect', () => {
        if (this.currentGroup) {
          this.currentGroup.setUserStatus(this, UserStatus.Offline);
          const nick = this.currentGroup.nickNames.get(this);
          this.currentGroup.notify(`${nick} left the chat.`, memberLeft({ member: nick }));
        }
        this.deleteTimeout = setTimeout(() => {
          this.onDeleted();
          this.groupsVisited.forEach((g: Group) => {
            g.notify(null, memberDeleted({ member: g.nickNames.get(this) }));
            g.setUserStatus(this, UserStatus.Deleted);
          });
        }, 20 * 60 * 1000);
      });
    }

    if (this.preKeysNeeded) {
      this.socket.emit(SignalSocketMessages.NewPrekeysRequired);
      this.preKeysNeeded = false;
    }
  }

  public promptForPrekeys() {
    if (this.socket.connected) {
      this.socket.emit(SignalSocketMessages.NewPrekeysRequired);
    } else {
      this.preKeysNeeded = true;
    }
  }
}
