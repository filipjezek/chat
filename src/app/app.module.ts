import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule, Injector, Injectable } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { createCustomElement } from '@angular/elements';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import {
  faSatellite,
  faPaperPlane,
  faEllipsisV,
  faTimes,
  faEye,
  faEyeSlash,
  faUserTimes,
  faAngleRight,
  faArrowLeft,
} from '@fortawesome/free-solid-svg-icons';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { ROOT_REDUCER, metaReducers } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { LoginPageComponent } from './login-page/login-page.component';
import { SelectPageComponent } from './select-page/select-page.component';
import { RoomPageComponent } from './room-page/room-page.component';
import { InputComponent } from './widgets/input/input.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonComponent } from './widgets/button/button.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CreateRoomComponent } from './select-page/create-room/create-room.component';
import { JoinRoomComponent } from './select-page/join-room/join-room.component';
import { PassphraseDialogComponent } from './dialogs/passphrase-dialog/passphrase-dialog.component';
import { OverlayComponent } from './overlay/overlay.component';
import { environment } from 'src/environments/environment';
import { AuthEffects } from './store/effects/auth.effects';
import { RoomEffects } from './store/effects/room.effects';
import { MessageComponent } from './room-page/chat-container/message/message.component';
import { MessageInputComponent } from './room-page/bottom-bar/message-input/message-input.component';
import { TopBarComponent } from './room-page/top-bar/top-bar.component';
import { KebabMenuComponent } from './room-page/bottom-bar/kebab-menu/kebab-menu.component';
import { LoadingOverlayComponent } from './loading-overlay/loading-overlay.component';
import { ChatContainerComponent } from './room-page/chat-container/chat-container.component';
import { NotificationComponent } from './room-page/chat-container/notification/notification.component';
import { BottomBarComponent } from './room-page/bottom-bar/bottom-bar.component';
import { KebabMenuItemComponent } from './room-page/bottom-bar/kebab-menu/kebab-menu-item/kebab-menu-item.component';
import { TextboxDialogComponent } from './dialogs/textbox-dialog/textbox-dialog.component';
import { ChangePassphraseDialogComponent } from './dialogs/change-passphrase-dialog/change-passphrase-dialog.component';
import { UsersSelectDialogComponent } from './dialogs/users-select-dialog/users-select-dialog.component';
import { UserComponent } from './dialogs/users-select-dialog/user/user.component';
import { SettingsDialogComponent } from './dialogs/settings-dialog/settings-dialog.component';
import { SettingsEffects } from './store/effects/settings.effects';
import { UiEffects } from './store/effects/ui.effects';

@Injectable()
export class ChatHammerConfig extends HammerGestureConfig {
  buildHammer(element: HTMLElement) {
    delete Hammer.defaults.cssProps.userSelect;
    const mc = new Hammer(element);
    return mc;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    SelectPageComponent,
    RoomPageComponent,
    InputComponent,
    ButtonComponent,
    NotFoundComponent,
    CreateRoomComponent,
    JoinRoomComponent,
    PassphraseDialogComponent,
    OverlayComponent,
    MessageComponent,
    MessageInputComponent,
    TopBarComponent,
    KebabMenuComponent,
    LoadingOverlayComponent,
    ChatContainerComponent,
    NotificationComponent,
    BottomBarComponent,
    KebabMenuItemComponent,
    TextboxDialogComponent,
    ChangePassphraseDialogComponent,
    UsersSelectDialogComponent,
    UserComponent,
    SettingsDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(ROOT_REDUCER, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      },
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : /*[]*/ StoreDevtoolsModule.instrument(), // for illustration purposes
    EffectsModule.forRoot([AuthEffects, RoomEffects, SettingsEffects, UiEffects]),
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: ChatHammerConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(injector: Injector, library: FaIconLibrary) {
    library.addIcons(
      faSatellite,
      faPaperPlane,
      faEllipsisV,
      faTimes,
      faEye,
      faEyeSlash,
      faUserTimes,
      faAngleRight,
      faArrowLeft
    );

    const customEls = [
      PassphraseDialogComponent,
      TextboxDialogComponent,
      ChangePassphraseDialogComponent,
      UsersSelectDialogComponent,
      SettingsDialogComponent,
    ];

    for (const el of customEls) {
      const custEl = createCustomElement(el, { injector });
      customElements.define(el.selector, custEl as any); // for some reason the typing doesn't work
    }
  }
}
