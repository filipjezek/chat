import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Member } from 'src/shared/interfaces/member';

@Component({
  selector: 'chat-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  @Input() title = '';
  @Input() whisperingTo: Member;
  @Output() closePrivateConvo = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
