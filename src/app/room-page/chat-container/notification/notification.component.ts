import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Notification } from 'src/shared/interfaces/message';
import { colorFromString } from 'src/app/utils/color-from-string';

@Component({
  selector: 'chat-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationComponent implements OnInit {
  @Input() data: Notification;
  color: string;

  constructor() {}

  ngOnInit() {
    if (
      this.data.text.endsWith('the room.') ||
      this.data.text.endsWith('the chat.') ||
      this.data.text.endsWith('kicked out.') ||
      this.data.text.endsWith('the lock.')
    ) {
      this.color = colorFromString(this.data.text.substring(0, this.data.text.indexOf(' ')));
    }
  }
}
