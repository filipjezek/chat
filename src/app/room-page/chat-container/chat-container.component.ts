import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'chat-chat-container',
  templateUrl: './chat-container.component.html',
  styleUrls: ['./chat-container.component.scss']
})
export class ChatContainerComponent implements OnInit {
  constructor(private el: ElementRef<HTMLElement>) {}

  ngOnInit() {}

  public scrollDown() {
    this.el.nativeElement.scrollTop = this.el.nativeElement.scrollHeight;
  }
}
