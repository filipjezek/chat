import { Component, OnInit, Input, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import { colorFromString } from 'src/app/utils/color-from-string';
import { DecryptedMessage } from 'src/shared/interfaces/message';

@Component({
  selector: 'chat-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageComponent implements OnInit {
  @Input() @HostBinding('class.own') own: boolean;
  @Input() @HostBinding('class.continue') continue: boolean;
  @Input() data: DecryptedMessage;
  public color: string;

  constructor() {}

  ngOnInit() {
    this.color = colorFromString(this.data.author);
  }
}
