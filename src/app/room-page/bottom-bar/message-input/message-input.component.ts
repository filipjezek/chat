import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'chat-message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.scss']
})
export class MessageInputComponent implements OnInit {
  form = new FormGroup({
    message: new FormControl('', Validators.required)
  });

  @Output() sendMessage = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {}

  onSubmit() {
    this.sendMessage.next(this.form.get('message').value);
    this.form.get('message').setValue('');
  }
}
