import { Component, OnInit, Input } from '@angular/core';
import { Member } from 'src/shared/interfaces/member';

@Component({
  selector: 'chat-bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.scss']
})
export class BottomBarComponent implements OnInit {
  @Input() whisperingTo: Member;

  constructor() {}

  ngOnInit() {}
}
