import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ContentChildren,
  QueryList,
  AfterContentInit
} from '@angular/core';
import { GlobalEventService } from 'src/app/services/global-event.service';
import { merge, combineLatest } from 'rxjs';
import { filter, takeUntil, map } from 'rxjs/operators';
import { Unsubscriber } from 'src/app/unsubscriber';
import { promiseTimeout } from 'src/app/utils/promise-timeout';
import { trigger, transition, useAnimation } from '@angular/animations';
import { dropdownIn, dropdownOut } from 'src/app/animations';
import { KebabMenuItemComponent } from './kebab-menu-item/kebab-menu-item.component';

@Component({
  selector: 'chat-kebab-menu',
  templateUrl: './kebab-menu.component.html',
  styleUrls: ['./kebab-menu.component.scss'],
  animations: [
    trigger('dropdown', [
      transition(':enter', useAnimation(dropdownIn)),
      transition(':leave', useAnimation(dropdownOut))
    ])
  ]
})
export class KebabMenuComponent extends Unsubscriber implements OnInit, AfterContentInit {
  @ViewChild('menu') menu: ElementRef<HTMLElement>;
  @ContentChildren(KebabMenuItemComponent) items: QueryList<KebabMenuItemComponent>;

  showMenu = false;
  showAlert = false;

  constructor(private gES: GlobalEventService) {
    super();
  }

  ngOnInit() {
    merge(
      this.gES.documentClicked.pipe(
        filter(x => !(x.target as HTMLElement).closest('chat-kebab-menu > button'))
      ),
      this.gES.escapePressed
    )
      .pipe(
        filter(() => this.showMenu),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => (this.showMenu = false));
  }

  ngAfterContentInit() {
    this.items.changes.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      combineLatest(this.items.map(i => i.alertChanges))
        .pipe(
          takeUntil(this.onDestroy$),
          map(x => x.some(a => !!a))
        )
        .subscribe(b => (this.showAlert = b));
    });
    this.items.notifyOnChanges();
  }

  async openMenu() {
    this.showMenu = true;
    await promiseTimeout();
    this.menu.nativeElement.tabIndex = -1;
    this.menu.nativeElement.focus();
    this.menu.nativeElement.tabIndex = null;
  }
}
