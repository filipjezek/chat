import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KebabMenuItemComponent } from './kebab-menu-item.component';

describe('KebabMenuItemComponent', () => {
  let component: KebabMenuItemComponent;
  let fixture: ComponentFixture<KebabMenuItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KebabMenuItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KebabMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
