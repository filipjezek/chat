import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'chat-kebab-menu-item',
  templateUrl: './kebab-menu-item.component.html',
  styleUrls: ['./kebab-menu-item.component.scss']
})
export class KebabMenuItemComponent implements OnInit {
  @Input() get alert(): number {
    return this._alert;
  }
  set alert(a: number) {
    if (a != this._alert) {
      this.alertChanges.next(a);
      this._alert = a;
    }
  }
  private _alert = 0;
  alertChanges = new BehaviorSubject<number>(0);

  constructor() {}

  ngOnInit() {}
}
