import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, zip, merge, throwError } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../store/reducers';
import { ActivatedRoute } from '@angular/router';
import {
  tap,
  map,
  take,
  filter,
  switchMap,
  takeUntil,
  skipWhile,
  delay,
  distinctUntilChanged
} from 'rxjs/operators';
import {
  joinRoom,
  lockRoom,
  openPrivateConversation,
  closePrivateConversation,
  kickoutMember,
  sendMessage,
  roomPageErrorSeen,
  whisper
} from '../../shared/actions/room.actions';
import { DialogService } from '../services/dialog.service';
import { ToastService } from '../services/toast.service';
import { Unsubscriber } from '../unsubscriber';
import { TextboxDialogComponent } from '../dialogs/textbox-dialog/textbox-dialog.component';
import { ChangePassphraseDialogComponent } from '../dialogs/change-passphrase-dialog/change-passphrase-dialog.component';
import { UsersSelectDialogComponent } from '../dialogs/users-select-dialog/users-select-dialog.component';
import { SettingsDialogComponent } from '../dialogs/settings-dialog/settings-dialog.component';
import { updateSettings, loadSettings } from '../store/actions/settings.actions';
import { ChatContainerComponent } from './chat-container/chat-container.component';
import { IconService } from '../services/icon.service';
import { SoundService } from '../services/sound.service';
import { Notification, DecryptedMessage } from 'src/shared/interfaces/message';
import { passphraseMixin } from '../passphrase-mixin';
import { Member } from 'src/shared/interfaces/member';

@Component({
  selector: 'chat-room-page',
  templateUrl: './room-page.component.html',
  styleUrls: ['./room-page.component.scss']
})
export class RoomPageComponent extends passphraseMixin(Unsubscriber) implements OnInit {
  @ViewChild(ChatContainerComponent, { static: true }) chatContainer: ChatContainerComponent;
  roomID$: Observable<string>;
  messages$: Observable<(DecryptedMessage | Notification)[]>;
  whisperingTo$: Observable<Member>;
  me$: Observable<string>;
  isAdmin$: Observable<boolean>;
  apiError$: Observable<any>;
  unreadUserCount$: Observable<number>;

  constructor(
    store: Store<State>,
    route: ActivatedRoute,
    dialogS: DialogService,
    private toastS: ToastService,
    private icon: IconService,
    private soundS: SoundService
  ) {
    super(store, dialogS);
    this.roomID$ = zip(
      store.select(x => x.room.currentRoom && x.room.currentRoom.id),
      route.paramMap.pipe(map(x => x.get('id')))
    ).pipe(
      tap(arr => {
        if (arr[0] !== arr[1]) {
          this.store.dispatch(loadSettings({ room: arr[1] || 'testingRoom123' }));
          store.dispatch(joinRoom({ roomID: arr[1] }));
        }
      }),
      map(arr => arr[1])
    );

    this.messages$ = this.store.select(x =>
      x.room.currentRoom
        ? x.room.whisperingTo
          ? x.room.currentRoom.members.find(m => m.nick === x.room.whisperingTo).privateMessages
          : x.room.currentRoom.messages
        : []
    );
    this.me$ = this.store.select(x => x.room.nickMask);
    this.isAdmin$ = this.store.select(x => x.room.currentRoom.isAdmin);
    this.whisperingTo$ = this.store.select(
      x => x.room.whisperingTo && x.room.currentRoom.members.find(m => m.nick === x.room.whisperingTo)
    );
    this.apiError$ = this.store.select(x => x.room.error);
    this.unreadUserCount$ = this.store
      .select(x => x.room.currentRoom.members)
      .pipe(
        distinctUntilChanged(),
        map(members => members.reduce((prev, curr) => prev + (curr.unreadMessages && 1), 0))
      );
  }

  ngOnInit() {
    super.ngOnInit();
    this.messages$
      .pipe(
        skipWhile(m => !m.length),
        switchMap(() =>
          this.store
            .select(x => x.settings[x.room.currentRoom.id])
            .pipe(
              skipWhile(x => x === undefined),
              take(1)
            )
        ),
        takeUntil(this.onDestroy$),
        delay(0)
      )
      .subscribe(({ scroll, blink, sound }) => {
        if (scroll) {
          this.chatContainer.scrollDown();
        }
        if (!document.hasFocus()) {
          if (sound) {
            this.soundS.beep();
          }
          if (blink) {
            this.icon.blink();
          }
        }
      });
    this.apiError$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(e => !!e)
      )
      .subscribe(e => {
        this.toastS.add({ level: 'danger', message: e });
        console.error(e);
        this.store.dispatch(roomPageErrorSeen());
      });
  }

  shareRoom() {
    this.store
      .select(x => x.room.currentRoom.id)
      .pipe(take(1))
      .subscribe(id => {
        const ref = this.dialogS.open(TextboxDialogComponent);
        ref.link = id;
        ref.addEventListener('close', () => this.dialogS.close());
        ref.addEventListener('value', (e: CustomEvent) => {
          if (e.detail) {
            this.toastS.add({ level: 'normal', message: 'Copied to clipboard' });
            this.dialogS.close();
          } else {
            this.toastS.add({ level: 'danger', message: 'Failed to copy text to clipboard' });
          }
        });
      });
  }

  showLock() {
    this.store
      .select(x => x.room.currentRoom.lock)
      .pipe(take(1))
      .subscribe(lock => {
        const ref = this.dialogS.open(ChangePassphraseDialogComponent);
        ref.lock = lock || '';
        ref.addEventListener('close', () => this.dialogS.close());
        ref.addEventListener('value', (e: CustomEvent) => {
          if (e.detail !== lock) {
            this.store.dispatch(lockRoom({ passphrase: e.detail }));
            ref.loading = true;
            merge(
              this.store.select(x => x.room.currentRoom.lock).pipe(filter(l => l === e.detail)),
              this.store
                .select(x => x.room.error)
                .pipe(
                  filter(x => !!x),
                  switchMap(x => throwError(x))
                )
            )
              .pipe(take(1), takeUntil(this.onDestroy$))
              .subscribe(
                () => {
                  this.dialogS.close();
                },
                e => {
                  console.log(e);
                  ref.setAttribute('loading', 'false');
                  this.toastS.add({ level: 'danger', message: 'Failed to set the lock' });
                }
              );
          } else {
            this.dialogS.close();
          }
        });
      });
  }

  showWhisperDialog() {
    const ref = this.dialogS.openUnattached(UsersSelectDialogComponent);
    ref.users = this.store.select(x => x.room.currentRoom.members);
    ref.attach();

    ref.addEventListener('close', () => {
      this.dialogS.close();
    });
    ref.addEventListener('value', (e: CustomEvent) => {
      this.dialogS.close();
      this.store.dispatch(openPrivateConversation({ member: e.detail }));
    });
  }

  closePrivateConvo() {
    this.store.dispatch(closePrivateConversation());
  }

  manageUsers() {
    const ref = this.dialogS.openUnattached(UsersSelectDialogComponent);
    ref.type = 'manage';
    ref.users = this.store.select(x => x.room.currentRoom.members);
    ref.attach();

    ref.addEventListener('close', () => {
      this.dialogS.close();
    });
    ref.addEventListener('value', (e: CustomEvent) => {
      this.store.dispatch(kickoutMember({ member: e.detail }));
    });
  }

  openSettings() {
    this.roomID$
      .pipe(
        switchMap(id => this.store.select(x => ({ roomID: id, settings: x.settings[id] }))),
        take(1)
      )
      .subscribe(({ roomID, settings }) => {
        const ref = this.dialogS.open(SettingsDialogComponent);
        ref.initialValue = settings;
        ref.addEventListener('close', () => this.dialogS.close());
        ref.addEventListener('value', (e: CustomEvent) => {
          this.store.dispatch(updateSettings({ room: roomID, settings: e.detail }));
          this.dialogS.close();
        });
      });
  }

  sendMessage(message: string) {
    this.whisperingTo$.pipe(take(1)).subscribe(whisperingTo => {
      if (whisperingTo) {
        this.store.dispatch(whisper({ message, to: whisperingTo.nick }));
      } else {
        this.store.dispatch(sendMessage({ message }));
      }
    });
  }

  isMessage(msg: Notification | DecryptedMessage): msg is DecryptedMessage {
    return 'author' in msg;
  }
}
