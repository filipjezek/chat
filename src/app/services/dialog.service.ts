import { Injectable, Inject, ElementRef } from '@angular/core';
import { Subject, merge } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { promiseTimeout } from '../utils/promise-timeout';
import { GlobalEventService } from './global-event.service';
import { Store } from '@ngrx/store';
import { State } from '../store/reducers';
import { overlayOpen, overlayClose } from '../store/actions/ui.actions';
import { DOCUMENT } from '@angular/common';
import { WindowService } from './window.service';
import { Dialog as DialogClass } from '../dialogs/dialog';
import { NgElement, WithProperties } from '@angular/elements';

interface Dialog {
  el: HTMLElement;
  implicitlyClosable: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  private dEls: Dialog[] = [];
  private closed = new Subject<HTMLElement>();
  private initialOverlay: {
    // in case there was already overlay present on page
    open: boolean;
    zIndex: number;
    opacity: number;
  };
  private bodyScrollPos: number; // in order to prevent scrolling of the whole page underneath

  public get openCount(): number {
    return this.dEls.length;
  }

  constructor(
    globalS: GlobalEventService,
    private store: Store<State>,
    @Inject(DOCUMENT) private doc: Document,
    private windowS: WindowService
  ) {
    merge(
      globalS.escapePressed,
      globalS.documentClicked.pipe(filter(e => !!(e.target as HTMLElement).tagName.match(/^chat-overlay$/i)))
    )
      .pipe(filter(() => this.dEls.length && this.dEls[this.dEls.length - 1].implicitlyClosable))
      .subscribe(() => this.close());
  }

  open<T extends DialogClass>(
    el: (new (el: ElementRef, ...args: any[]) => T) & { selector: string },
    implicitlyClosable = true
  ): NgElement & WithProperties<T> {
    const ref = this.openUnattached(el, implicitlyClosable);
    ref.attach();
    delete ref.attach;

    return ref;
  }

  openUnattached<T extends DialogClass>(
    el: (new (el: ElementRef, ...args: any[]) => T) & { selector: string },
    implicitlyClosable = true
  ): NgElement & WithProperties<T> & { attach: () => void } {
    if (this.dEls.length === 0) {
      this.store
        .select(x => x.ui.overlay)
        .pipe(take(1))
        .subscribe(x => (this.initialOverlay = x));

      this.bodyScrollPos = this.doc.body.getBoundingClientRect().top;
      this.doc.body.style.position = 'fixed';
      this.doc.body.style.top = `${this.bodyScrollPos}px`;
    }

    this.store.dispatch(overlayOpen({ overlay: { zIndex: this.dEls.length * 2 + 5 } }));
    const dialogEl: NgElement & WithProperties<T> & { attach: () => void } = this.doc.createElement(
      el.selector
    ) as any;
    dialogEl.setAttribute('tabindex', '-1');
    dialogEl.style.position = 'fixed';
    dialogEl.style.left = '50%';
    dialogEl.style.transform = 'translate(-50%, -50%)';
    dialogEl.style.top = '50%';
    dialogEl.style.zIndex = this.dEls.length * 2 + 6 + '';
    dialogEl.style.maxHeight = '95%';
    dialogEl.style.overflowY = 'auto';
    dialogEl.style.overflowX = 'hidden';

    dialogEl.attach = () => {
      this.doc.body.appendChild(dialogEl);
      dialogEl.focus();
      dialogEl.removeAttribute('tabindex');
      this.dEls.push({
        el: dialogEl,
        implicitlyClosable: implicitlyClosable
      });
    };
    return dialogEl;
  }

  async close() {
    if (!this.dEls.length) {
      return null;
    }
    const current = this.dEls[this.dEls.length - 1];
    this.closed.next(current.el);
    current.el.setAttribute('animation-prop', 'closing');
    await promiseTimeout(this.dEls.length ? 150 : 120);
    this.doc.body.removeChild(current.el);
    this.dEls.pop();

    if (this.dEls.length === 0) {
      if (!this.initialOverlay.open) {
        this.store.dispatch(overlayClose());
      } else {
        this.store.dispatch(
          overlayOpen({
            overlay: { zIndex: this.initialOverlay.zIndex, opacity: this.initialOverlay.opacity }
          })
        );
      }
      this.doc.body.style.position = null;
      this.doc.body.style.top = null;
      this.doc.body.style.paddingRight = null;
      this.windowS.window.scrollTo(0, -this.bodyScrollPos);
    } else {
      this.store.dispatch(overlayOpen({ overlay: { zIndex: (this.dEls.length - 1) * 2 + 5 } }));
    }
  }

  async closeAll() {
    while (this.dEls.length) {
      await this.close();
    }
  }
}
