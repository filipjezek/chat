import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Subject, Observable } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { State } from '../store/reducers';
import {
  notificationReceived,
  memberJoined,
  memberKickedOut,
  memberLeft,
  roomLocked,
  memberDeleted,
  whisperReceivedEncrypted,
  messageReceivedEncrypted
} from '../../shared/actions/room.actions';
import { ofType } from '@ngrx/effects';
import { filter, switchMap } from 'rxjs/operators';
import { SocketMessages, SignalSocketMessages } from 'src/shared/interfaces/socket-messages';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socket: SocketIOClient.Socket;
  private eventsSubj = new Subject<Action>();

  public get events(): Observable<Action> {
    return this.eventsSubj.asObservable();
  }

  constructor(store: Store<State>) {
    this.socket = io(`${location.protocol}//${location.hostname}:3000`);
    this.socket.on('ngrx action', (action: Action) => {
      this.eventsSubj.next(action);
    });
    store
      .select(x => x.room)
      .pipe(
        filter(room => room.currentRoom && !room.joinInProgress),
        switchMap(() => {
          return this.eventsSubj.pipe(
            ofType(
              messageReceivedEncrypted,
              notificationReceived,
              whisperReceivedEncrypted,
              memberJoined,
              memberKickedOut,
              memberLeft,
              memberDeleted,
              roomLocked
            )
          );
        })
      )
      .subscribe(a => store.dispatch(a));
  }

  public send(event: SocketMessages | SignalSocketMessages, ...payload: any[]) {
    this.socket.emit(event, ...payload);
  }

  public onSignal(event: SignalSocketMessages, cb: (...args: any[]) => void) {
    this.socket.on(event, cb);
  }
}
