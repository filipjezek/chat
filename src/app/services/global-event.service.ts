import { Injectable, Inject } from '@angular/core';
import { Observable, fromEvent, merge } from 'rxjs';
import { filter, throttleTime } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class GlobalEventService {
  public documentClicked: Observable<MouseEvent | HammerInput>;
  public keyPressed: Observable<KeyboardEvent>;
  public enterPressed: Observable<KeyboardEvent>;
  public spacePressed: Observable<KeyboardEvent>;
  public escapePressed: Observable<KeyboardEvent>;

  constructor(
    @Inject(DOCUMENT) private doc: Document,
    @Inject(HAMMER_GESTURE_CONFIG) hammer: HammerGestureConfig
  ) {
    const hammerTime = hammer.buildHammer(this.doc.querySelector(':root'));
    this.documentClicked = merge(
      fromEvent<MouseEvent>(this.doc, 'click'),
      fromEvent<HammerInput>(hammerTime, 'tap')
    ).pipe(throttleTime(5));

    this.keyPressed = fromEvent<KeyboardEvent>(this.doc, 'keydown');
    this.enterPressed = this.keyPressed.pipe(filter(e => e.which === 13 || e.key === 'Enter'));
    this.spacePressed = this.keyPressed.pipe(
      filter(e => e.which === 32 || e.key === ' ' || e.key === 'Spacebar')
    );
    this.escapePressed = this.keyPressed.pipe(filter(e => e.which === 27 || e.key === 'Escape'));
  }
}
