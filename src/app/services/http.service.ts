import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpParams, HttpRequest } from '@angular/common/http';
import { retryWhen, concatMap, delay, map, tap, pluck } from 'rxjs/operators';

export interface HttpReq<T> {
  data: T;
  error?: string;
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  public apiUrl = 'api/';

  private retryPipeline = retryWhen<HttpReq<any>>(err =>
    err.pipe(
      concatMap((e: HttpErrorResponse, i) => {
        if (i > 5 || ![503, 504, 0, 429, 425].includes(e.status)) {
          return throwError(e);
        }
        return of(e).pipe(delay(i * 500));
      })
    )
  );

  constructor(private http: HttpClient) {}

  get<T>(url: string, params?: HttpParams): Observable<T> {
    return this.http
      .get<HttpReq<T>>(this.apiUrl + url, {
        params: params
      })
      .pipe(
        this.retryPipeline,
        pluck('data')
      );
  }

  post<T>(url: string, body: any): Observable<T> {
    return this.http.post<HttpReq<T>>(this.apiUrl + url, body).pipe(
      this.retryPipeline,
      pluck('data')
    );
  }

  put(url: string, body: any): Observable<void> {
    return this.http.put<HttpReq<void>>(this.apiUrl + url, body).pipe(
      this.retryPipeline,
      pluck('data')
    );
  }

  delete(url: string, params?: HttpParams): Observable<void> {
    return this.http
      .delete<HttpReq<void>>(this.apiUrl + url, {
        params: params
      })
      .pipe(
        this.retryPipeline,
        pluck('data')
      );
  }

  postFile(url: string, file: File, filename: string) {
    const data = new FormData();
    data.append(filename, file);
    return this.http.post(this.apiUrl + url, data).pipe(
      this.retryPipeline,
      pluck('data')
    );
  }
}
