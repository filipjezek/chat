import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SoundService {
  private beepSound: HTMLAudioElement;

  constructor() {
    this.beepSound = new Audio('/assets/beep.mp3');
  }

  public beep() {
    this.beepSound.currentTime = 0;
    this.beepSound.play();
  }
}
