import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ClipboardService {
  constructor(@Inject(DOCUMENT) private doc: Document) {}

  /**
   * Tries to copy currently selected text
   * @returns boolean; true if the operation was successful,
   * false otherwise
   */
  public copy(): boolean {
    try {
      return this.doc.execCommand('copy');
    } catch (error) {
      return false;
    }
  }
}
