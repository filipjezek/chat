import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../store/reducers';
import { take } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IconService {
  public setIcon(name: string) {
    document.querySelector('link[rel=icon]').setAttribute('href', `/assets/icons/${name}.ico`);
  }

  public blink() {
    this.store
      .select(x => x.settings[x.room.currentRoom.id].icon)
      .pipe(take(1))
      .subscribe(icon => {
        if (icon === 'default') {
          this.setIcon(`${icon}-alert`);
          fromEvent(window, 'focus')
            .pipe(take(1))
            .subscribe(() => {
              this.setIcon(icon);
            });
        }
      });
  }

  constructor(private store: Store<State>) {}
}
