import { Injectable } from '@angular/core';
import * as Cookies from 'js-cookie';
import { WindowService } from './window.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private useLocalStorage = true;
  private useSessionStorage = true;

  constructor(private windowS: WindowService) {
    this.useLocalStorage = this.storageAvailable('localStorage');
    this.useSessionStorage = this.storageAvailable('sessionStorage');
  }

  private storageAvailable(type: string) {
    let storage;
    try {
      storage = this.windowS.window[type];
      const x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      return true;
    } catch (e) {
      return (
        e instanceof DOMException &&
        // everything except Firefox
        (e.code === 22 ||
          // Firefox
          e.code === 1014 ||
          // test name field too, because code might not be present
          // everything except Firefox
          e.name === 'QuotaExceededError' ||
          // Firefox
          e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
        // acknowledge QuotaExceededError only if there's something already stored
        storage && storage.length !== 0
      );
    }
  }

  public setLocal<T>(key: string, value: T) {
    if (this.useLocalStorage) {
      this.windowS.window.localStorage.setItem(key, JSON.stringify(value));
    } else {
      Cookies.set(key, value as any, { expires: 365 });
    }
  }

  public getLocal<T>(key: string, fallback?: T): T {
    let res;
    if (this.useLocalStorage) {
      res = JSON.parse(this.windowS.window.localStorage.getItem(key));
    } else {
      res = Cookies.getJSON(key);
    }
    return res ? res : fallback;
  }

  public removeLocal(key: string) {
    if (this.useLocalStorage) {
      this.windowS.window.localStorage.removeItem(key);
    } else {
      Cookies.remove(key);
    }
  }

  public setSession<T>(key: string, value: T) {
    if (this.useSessionStorage) {
      this.windowS.window.sessionStorage.setItem(key, JSON.stringify(value));
    } else {
      Cookies.set(key, value as any);
    }
  }

  public getSession<T>(key: string, fallback?: T): T {
    let res;
    if (this.useSessionStorage) {
      res = JSON.parse(this.windowS.window.sessionStorage.getItem(key));
    } else {
      res = Cookies.getJSON(key);
    }
    return res ? res : fallback;
  }

  public removeSession(key: string) {
    if (this.useSessionStorage) {
      this.windowS.window.sessionStorage.removeItem(key);
    } else {
      Cookies.remove(key);
    }
  }
}
