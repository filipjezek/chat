export interface Toast {
  message: string;
  level: 'normal' | 'success' | 'warning' | 'danger';
}
