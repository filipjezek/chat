import { Injectable } from '@angular/core';
import {
  EncryptedMessage,
  Notification,
  SerializedInternalMessage,
  MsgStub,
  DecryptedMessage
} from 'src/shared/interfaces/message';
import { SignalProtocolStore, KeyPair } from '../libsignal/signal-protocol-store';
import { Store } from '@ngrx/store';
import { State } from '../store/reducers';
import {
  filter,
  switchMap,
  distinctUntilChanged,
  skipWhile,
  take,
  withLatestFrom,
  exhaustMap,
  map,
  concatMap
} from 'rxjs/operators';
import { Observable, from, zip, of } from 'rxjs';
import { SocketService } from './socket.service';
import { UserStatus } from 'src/shared/interfaces/user-status';
import * as ByteBuffer from 'bytebuffer';
import { preKeyBundle, preKey } from 'src/shared/interfaces/prekeys';
import { SignalSocketMessages } from 'src/shared/interfaces/socket-messages';
import { Member } from 'src/shared/interfaces/member';

const libsignal = (window as any).libsignal;

interface cipher {
  encrypt: (plaintext: string | ArrayBuffer, encoding?: string) => Promise<ArrayBuffer>;
  decryptPreKeyWhisperMessage: (ciphertext: string | ArrayBuffer, encoding?: string) => Promise<ArrayBuffer>;
  decryptWhisperMessage: (ciphertext: string | ArrayBuffer, encoding?: string) => Promise<ArrayBuffer>;
}

@Injectable({
  providedIn: 'root'
})
export class EncryptionService {
  private signalStore: SignalProtocolStore;
  private loggedIn$: Observable<boolean>;
  private members$: Observable<[Member[], string]>;
  private ciphers: Record<string, cipher> = {};

  constructor(private store: Store<State>, private socket: SocketService) {
    this.signalStore = new SignalProtocolStore();
    this.loggedIn$ = this.store
      .select(x => x.auth.authInProgress)
      .pipe(
        skipWhile(x => !x),
        filter(x => !x),
        switchMap(() => this.store.select(x => !!x.auth.user)),
        distinctUntilChanged()
      );

    this.loggedIn$.pipe(filter(x => !x)).subscribe(() => {
      this.signalStore.clear();
    });

    this.socket.onSignal(SignalSocketMessages.NewPrekeysRequired, () => {
      this.generateNewPrekeys().then(prekeys =>
        this.socket.send(SignalSocketMessages.PublishNewPrekeys, prekeys)
      );
    });

    this.members$ = this.store
      .select(x => x.room.currentRoom.members)
      .pipe(take(1), withLatestFrom(this.store.select(x => x.room.currentRoom.id)));
  }

  public async initSignal(): Promise<preKeyBundle> {
    const registrationId = libsignal.KeyHelper.generateRegistrationId();
    this.signalStore.setLocalRegistrationId(registrationId);
    const initializationPromises: Promise<any>[] = [];

    initializationPromises.push(
      libsignal.KeyHelper.generateIdentityKeyPair()
        .then((identityKeyPair: KeyPair) => {
          this.signalStore.setIdentityKeyPair(identityKeyPair);
          return libsignal.KeyHelper.generateSignedPreKey(identityKeyPair, 1).then(signedPreKey => [
            identityKeyPair.pubKey,
            signedPreKey
          ]);
        })
        .then(([identityKey, signedPreKey]) => {
          return this.signalStore.storeSignedPreKey(signedPreKey.keyId, signedPreKey.keyPair).then(() => {
            signedPreKey.publicKey = signedPreKey.keyPair.pubKey;
            delete signedPreKey.keyPair;
            return [identityKey, signedPreKey];
          });
        })
    );
    initializationPromises.push(this.generateNewPrekeys());

    const args = await Promise.all(initializationPromises);
    return {
      registrationId,
      identityKey: args[0][0],
      signedPreKey: args[0][1],
      preKeys: args[1]
    };
  }

  private generateNewPrekeys(): Promise<preKey[]> {
    const promises = [];
    const lastPrekeyId = this.signalStore.lastPrekeyId;
    for (let i = lastPrekeyId + 1; i < lastPrekeyId + 16; i++) {
      promises.push(
        libsignal.KeyHelper.generatePreKey(i).then(preKey =>
          this.signalStore.storePreKey(preKey.keyId, preKey.keyPair).then(() => {
            preKey.publicKey = preKey.keyPair.pubKey;
            delete preKey.keyPair;
            return preKey;
          })
        )
      );
    }
    return Promise.all(promises);
  }

  public decryptMessages(
    m: (EncryptedMessage | Notification | MsgStub)[],
    nick: string,
    channel?: string
  ): Observable<(DecryptedMessage | Notification)[]> {
    let i = 0;
    let j = 0;
    return this.store
      .select(x => x.room.currentRoom.id)
      .pipe(
        take(1),
        switchMap(id =>
          zip(
            ...m.map(msg => {
              if ('ciphertext' in msg) {
                return this.decryptMessage(msg, i++, channel);
              } else if (!('text' in msg)) {
                return of({
                  text: this.signalStore.loadOwnMessage(id, j++, channel),
                  author: nick,
                  date: msg.date
                });
              }
              return of({ ...msg, type: 'notification' });
            })
          )
        )
      );
  }

  public decryptMessage(
    msg: EncryptedMessage,
    index?: number,
    channel?: string
  ): Observable<DecryptedMessage> {
    return this.store
      .select(x => x.room.currentRoom.id)
      .pipe(
        take(1),
        switchMap(id => {
          const address = id + '#' + msg.author;

          if (index !== undefined) {
            const loaded = this.signalStore.loadMessageKey(id, index, channel);
            if (loaded) {
              return of(ByteBuffer.wrap(loaded, 'binary').toArrayBuffer());
            }
          }

          if (!(address in this.ciphers)) {
            const protoAddress = new libsignal.SignalProtocolAddress(address, 1);
            this.ciphers[address] = new libsignal.SessionCipher(this.signalStore, protoAddress);
          }
          return from(
            this.ciphers[address][
              msg.key.type === 3 ? 'decryptPreKeyWhisperMessage' : 'decryptWhisperMessage'
            ](msg.key.body, 'binary')
          ).pipe(
            map(key => {
              this.signalStore.storeMessageKey(id, ByteBuffer.wrap(key).toString('binary'), channel);
              return key;
            })
          );
        }),
        concatMap(key => {
          return from(
            crypto.subtle.importKey('raw', key, 'AES-GCM', false, ['encrypt', 'decrypt']).then(key =>
              crypto.subtle.decrypt(
                {
                  name: 'AES-GCM',
                  iv: msg.iv
                },
                key,
                msg.ciphertext
              )
            )
          );
        }),
        map(plaintext => ({
          text: ByteBuffer.wrap(plaintext).toString('utf8'),
          date: msg.date,
          author: msg.author
        }))
      );
  }

  public encryptMessage(
    msg: string,
    nicks?: string[],
    isPrivate = false
  ): Observable<SerializedInternalMessage> {
    return this.members$.pipe(
      exhaustMap(([members, id]) => {
        if (!nicks) {
          nicks = [];
          members.forEach(m => {
            if (m.status === UserStatus.Online || m.status === UserStatus.Offline) {
              nicks.push(m.nick);
            }
          });
        }
        this.signalStore.storeOwnMessage(id, msg, isPrivate ? nicks[0] : undefined);
        const plaintext = ByteBuffer.wrap(msg).toArrayBuffer();
        const iv = crypto.getRandomValues(new Uint8Array(16));
        return from(
          crypto.subtle
            .generateKey(
              {
                name: 'AES-GCM',
                length: 256
              },
              true,
              ['encrypt', 'decrypt']
            )
            .then(k => {
              return Promise.all([
                crypto.subtle.encrypt({ name: 'AES-GCM', iv }, k, plaintext),
                crypto.subtle
                  .exportKey('raw', k)
                  .then(k => {
                    return ByteBuffer.wrap(k).toString('binary');
                  })
                  .then(k => this.encryptKeys(k, id, nicks))
              ]);
            })
            .then(args => new SerializedInternalMessage(args[0], args[1], iv, isPrivate))
        );
      })
    );
  }

  private encryptKeys(k: string, id: string, members: string[]) {
    const p: Promise<[string, ArrayBuffer]>[] = [];
    const request = [];
    members.forEach(m => {
      const address = id + '#' + m;
      const protoAddress = new libsignal.SignalProtocolAddress(address, 1);
      if (!(address in this.ciphers)) {
        this.ciphers[address] = new libsignal.SessionCipher(this.signalStore, protoAddress);
      }

      if (this.signalStore.hasSession(protoAddress.toString())) {
        p.push(this.ciphers[address].encrypt(k, 'binary').then(c => [m, c]));
      } else {
        request.push(m);
      }
    });

    if (request.length) {
      this.socket.send(SignalSocketMessages.RequestBundles, request);
      return new Promise<[string, preKeyBundle][]>((res, rej) => {
        this.socket.onSignal(SignalSocketMessages.Bundles, res);
      }).then(bundles =>
        Promise.all(
          p.concat(
            bundles.map(([nick, bundle]) => {
              const address = id + '#' + nick;
              const protoAddress = new libsignal.SignalProtocolAddress(address, 1);
              return new libsignal.SessionBuilder(this.signalStore, protoAddress)
                .processPreKey(bundle)
                .then(() => {
                  return this.ciphers[address].encrypt(k, 'binary').then(c => [nick, c]);
                })
                .catch(e => {
                  console.log(e);
                  alert(
                    `${e.message ||
                      e}\n${nick}'s identity token was corrupted. ${nick} will not receive the message.`
                  );
                });
            })
          )
        )
      );
    }

    return Promise.all(p);
  }
}
