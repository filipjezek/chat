import { Injectable, Inject } from '@angular/core';
import { Toast } from './toast';
import { promiseTimeout } from '../utils/promise-timeout';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  private toasts: Toast[] = [];
  private currentToast: Toast;
  private toastTimeout: any;

  constructor(@Inject(DOCUMENT) private doc: Document) {}

  public add(t: Toast) {
    this.toasts.push(t);
    if (!this.currentToast) {
      this.show();
    }
  }

  private async show() {
    this.currentToast = this.toasts.shift();
    const ref = this.doc.createElement('div');
    ref.id = 'chat-toast';
    ref.style.cssText = `
    position:fixed;
    bottom:calc(10% - 15px);
    left:50%;
    transform:translateX(-50%);
    max-width: 300px;
    min-width: 220px;
    border-radius:4px;
    box-shadow:1px 1px 4px 0px #00000057;
    padding:12px;
    color:white;
    text-align:center;
    transition:opacity 0.2s ease-out, bottom 0.2s ease-out;
    opacity:0;
    z-index: 999;
    cursor: pointer;
    `;
    ref.textContent = this.currentToast.message;
    switch (this.currentToast.level) {
      case 'danger':
        ref.style.backgroundColor = '#ba0d0d';
        break;
      case 'success':
        ref.style.backgroundColor = '#0d900d';
        break;
      case 'warning':
        ref.style.backgroundColor = '#dd8d16';
        break;
      default:
        ref.style.backgroundColor = 'MediumPurple';
    }
    this.doc.body.appendChild(ref);
    ref.addEventListener('click', () => {
      clearTimeout(this.toastTimeout);
      this.changeToast();
    });
    await promiseTimeout();
    ref.style.opacity = '1';
    ref.style.bottom = '10%';
    this.toastTimeout = setTimeout(() => this.changeToast(), 6000);
  }
  private async hide() {
    const ref = this.doc.getElementById('chat-toast');
    ref.style.transition = 'opacity 0.2s ease-in, bottom 0.2s ease-in';
    ref.style.opacity = '0';
    ref.style.bottom = 'calc(10% - 15px)';
    await promiseTimeout(200);
    this.doc.body.removeChild(ref);
  }

  private async changeToast() {
    this.currentToast = null;
    await this.hide();
    if (this.toasts.length) {
      this.show();
    }
  }
}
