"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var libsignal = window['libsignal'];
var SignalProtocolStore = /** @class */ (function () {
    function SignalProtocolStore() {
        this.decryptedKeys = {};
        this.ownMessages = {};
        this.Direction = {
            SENDING: 1,
            RECEIVING: 2
        };
        // this.prefix = `signalstore${++SignalProtocolStore.instanceCount}.`;
        this.prefix = 'signalstore.';
        var stringifiedDecKeys = sessionStorage.getItem(this.prefix + "decryptedKeys");
        if (stringifiedDecKeys) {
            this.decryptedKeys = JSON.parse(stringifiedDecKeys);
        }
        var stringifiedOwnMsgs = sessionStorage.getItem(this.prefix + "ownMessages");
        if (stringifiedOwnMsgs) {
            this.ownMessages = JSON.parse(stringifiedOwnMsgs);
        }
    }
    Object.defineProperty(SignalProtocolStore.prototype, "lastPrekeyId", {
        get: function () {
            return +sessionStorage.getItem(this.prefix + "lastPrekey");
        },
        set: function (id) {
            sessionStorage.setItem(this.prefix + "lastPrekey", '' + id);
        },
        enumerable: true,
        configurable: true
    });
    SignalProtocolStore.prototype.clear = function () {
        for (var i = 0; i < sessionStorage.length; i++) {
            var key = sessionStorage.key(i);
            if (key.startsWith(this.prefix)) {
                sessionStorage.removeItem(key);
                i--;
            }
        }
    };
    SignalProtocolStore.prototype.bufferToStr = function (buffer) {
        return libsignal.util.toString(buffer);
    };
    SignalProtocolStore.prototype.strToBuffer = function (str) {
        return libsignal.util.toArrayBuffer(str);
    };
    SignalProtocolStore.prototype.saveKeyPair = function (name, keypair) {
        var obj = { pubKey: this.bufferToStr(keypair.pubKey), privKey: this.bufferToStr(keypair.privKey) };
        sessionStorage.setItem(name, JSON.stringify(obj));
    };
    SignalProtocolStore.prototype.getKeyPair = function (name) {
        var item = sessionStorage.getItem(name);
        if (item) {
            var obj = JSON.parse(item);
            obj.pubKey = this.strToBuffer(obj.pubKey);
            obj.privKey = this.strToBuffer(obj.privKey);
            return obj;
        }
        return undefined;
    };
    SignalProtocolStore.prototype.storeSession = function (address, record) {
        var obj = {};
        if (record instanceof ArrayBuffer) {
            obj['buffer'] = this.bufferToStr(record);
        }
        else {
            obj['string'] = record;
        }
        sessionStorage.setItem(this.prefix + "session." + address, JSON.stringify(obj));
        return Promise.resolve();
    };
    SignalProtocolStore.prototype.loadSession = function (address) {
        var item = sessionStorage.getItem(this.prefix + "session." + address);
        if (!item) {
            return Promise.resolve(undefined);
        }
        var record = JSON.parse(item);
        if ('string' in record) {
            return Promise.resolve(record.string);
        }
        return Promise.resolve(this.strToBuffer(record.buffer));
    };
    SignalProtocolStore.prototype.removeSession = function (address) {
        return Promise.resolve(sessionStorage.removeItem(this.prefix + "session." + address));
    };
    SignalProtocolStore.prototype.removeAllSessions = function (uuid) {
        for (var i = 0; i < sessionStorage.length; i++) {
            var key = sessionStorage.key(i);
            if (key.startsWith(this.prefix + "session." + uuid)) {
                sessionStorage.removeItem(key);
                i--;
            }
        }
        return Promise.resolve();
    };
    SignalProtocolStore.prototype.hasSession = function (address) {
        return !!sessionStorage.getItem(this.prefix + "session." + address);
    };
    SignalProtocolStore.prototype.getLocalRegistrationId = function () {
        return Promise.resolve(+sessionStorage.getItem(this.prefix + "registrationId"));
    };
    SignalProtocolStore.prototype.setLocalRegistrationId = function (id) {
        sessionStorage.setItem(this.prefix + "registrationId", id + '');
    };
    SignalProtocolStore.prototype.getIdentityKeyPair = function () {
        return Promise.resolve(this.getKeyPair(this.prefix + "identityKey"));
    };
    SignalProtocolStore.prototype.setIdentityKeyPair = function (keypair) {
        this.saveKeyPair(this.prefix + "identityKey", keypair);
    };
    SignalProtocolStore.prototype.saveIdentity = function (address, identityKey) {
        if (address === null || address === undefined) {
            throw new Error('Tried to put identity key for undefined/null key');
        }
        var protoAddress = new libsignal.SignalProtocolAddress.fromString(address);
        var existing = sessionStorage.getItem(this.prefix + "identityKey." + protoAddress.getName());
        var stringified = this.bufferToStr(identityKey);
        if (stringified.length === 2) {
            console.trace();
        }
        sessionStorage.setItem(this.prefix + "identityKey." + protoAddress.getName(), stringified);
        if (existing && stringified !== existing) {
            return Promise.resolve(true);
        }
        else {
            return Promise.resolve(false);
        }
    };
    SignalProtocolStore.prototype.loadIdentityKey = function (uuid) {
        if (uuid === null || uuid === undefined)
            throw new Error('Tried to get identity key for undefined/null key');
        return Promise.resolve(this.strToBuffer(sessionStorage.getItem(this.prefix + "identityKey." + uuid)));
    };
    SignalProtocolStore.prototype.isTrustedIdentity = function (uuid, identityKey) {
        if (uuid === null || uuid === undefined) {
            throw new Error('tried to check identity key for undefined/null key');
        }
        if (!(identityKey instanceof ArrayBuffer)) {
            throw new Error('Expected identityKey to be an ArrayBuffer');
        }
        var trusted = sessionStorage.getItem(this.prefix + "identityKey." + uuid);
        if (trusted === null) {
            return Promise.resolve(true);
        }
        return Promise.resolve(this.bufferToStr(identityKey) === trusted);
    };
    SignalProtocolStore.prototype.storePreKey = function (address, preKey) {
        this.lastPrekeyId = +address;
        return Promise.resolve(this.saveKeyPair(this.prefix + "preKey." + address, preKey));
    };
    SignalProtocolStore.prototype.loadPreKey = function (address) {
        return Promise.resolve(this.getKeyPair(this.prefix + "preKey." + address));
    };
    SignalProtocolStore.prototype.removePreKey = function (address) {
        return Promise.resolve(sessionStorage.removeItem(this.prefix + "preKey." + address));
    };
    SignalProtocolStore.prototype.storeSignedPreKey = function (keyId, signedKey) {
        return Promise.resolve(this.saveKeyPair(this.prefix + "signedKey." + keyId, signedKey));
    };
    SignalProtocolStore.prototype.loadSignedPreKey = function (keyId) {
        return Promise.resolve(this.getKeyPair(this.prefix + "signedKey." + keyId));
    };
    SignalProtocolStore.prototype.removeSignedPreKey = function (keyId) {
        return Promise.resolve(sessionStorage.removeItem(this.prefix + "signedKey." + keyId));
    };
    SignalProtocolStore.prototype.storeMessageKey = function (roomID, key, channel) {
        if (channel === void 0) { channel = '#public'; }
        if (!(roomID in this.decryptedKeys)) {
            this.decryptedKeys[roomID] = {};
        }
        if (!(channel in this.decryptedKeys[roomID])) {
            this.decryptedKeys[roomID][channel] = [];
        }
        this.decryptedKeys[roomID][channel].push(key);
        sessionStorage.setItem(this.prefix + "decryptedKeys", JSON.stringify(this.decryptedKeys));
    };
    SignalProtocolStore.prototype.loadMessageKey = function (roomID, index, channel) {
        if (channel === void 0) { channel = '#public'; }
        return (this.decryptedKeys[roomID] &&
            this.decryptedKeys[roomID][channel] &&
            this.decryptedKeys[roomID][channel][index]);
    };
    SignalProtocolStore.prototype.storeOwnMessage = function (roomID, message, channel) {
        if (channel === void 0) { channel = '#public'; }
        if (!(roomID in this.ownMessages)) {
            this.ownMessages[roomID] = {};
        }
        if (!(channel in this.ownMessages[roomID])) {
            this.ownMessages[roomID][channel] = [];
        }
        this.ownMessages[roomID][channel].push(message);
        sessionStorage.setItem(this.prefix + "ownMessages", JSON.stringify(this.ownMessages));
    };
    SignalProtocolStore.prototype.loadOwnMessage = function (roomID, index, channel) {
        if (channel === void 0) { channel = '#public'; }
        return (this.ownMessages[roomID] &&
            this.ownMessages[roomID][channel] &&
            this.ownMessages[roomID][channel][index]);
    };
    SignalProtocolStore.instanceCount = 0;
    return SignalProtocolStore;
}());
exports.SignalProtocolStore = SignalProtocolStore;
