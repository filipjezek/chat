export interface KeyPair {
  pubKey: ArrayBuffer;
  privKey: ArrayBuffer;
}

const libsignal = window['libsignal'];

export class SignalProtocolStore {
  static instanceCount = 0;

  get lastPrekeyId(): number {
    return +sessionStorage.getItem(`${this.prefix}lastPrekey`);
  }
  set lastPrekeyId(id: number) {
    sessionStorage.setItem(`${this.prefix}lastPrekey`, '' + id);
  }

  private prefix: string;
  private decryptedKeys: Record<string, Record<string, string[]>> = {};
  private ownMessages: Record<string, Record<string, string[]>> = {};
  public Direction = {
    SENDING: 1,
    RECEIVING: 2
  };

  constructor() {
    // this.prefix = `signalstore${++SignalProtocolStore.instanceCount}.`;
    this.prefix = 'signalstore.';
    const stringifiedDecKeys = sessionStorage.getItem(`${this.prefix}decryptedKeys`);
    if (stringifiedDecKeys) {
      this.decryptedKeys = JSON.parse(stringifiedDecKeys);
    }
    const stringifiedOwnMsgs = sessionStorage.getItem(`${this.prefix}ownMessages`);
    if (stringifiedOwnMsgs) {
      this.ownMessages = JSON.parse(stringifiedOwnMsgs);
    }
  }

  clear() {
    for (let i = 0; i < sessionStorage.length; i++) {
      const key = sessionStorage.key(i);
      if (key.startsWith(this.prefix)) {
        sessionStorage.removeItem(key);
        i--;
      }
    }
  }

  private bufferToStr(buffer: ArrayBuffer | string): string {
    return libsignal.util.toString(buffer);
  }
  private strToBuffer(str: string): ArrayBuffer {
    return libsignal.util.toArrayBuffer(str);
  }
  private saveKeyPair(name: string, keypair: KeyPair) {
    const obj = { pubKey: this.bufferToStr(keypair.pubKey), privKey: this.bufferToStr(keypair.privKey) };
    sessionStorage.setItem(name, JSON.stringify(obj));
  }
  private getKeyPair(name: string): KeyPair {
    const item = sessionStorage.getItem(name);
    if (item) {
      const obj = JSON.parse(item);
      obj.pubKey = this.strToBuffer(obj.pubKey);
      obj.privKey = this.strToBuffer(obj.privKey);
      return obj;
    }
    return undefined;
  }

  public storeSession(address: string, record: ArrayBuffer | string): Promise<void> {
    const obj = {};
    if (record instanceof ArrayBuffer) {
      obj['buffer'] = this.bufferToStr(record);
    } else {
      obj['string'] = record;
    }
    sessionStorage.setItem(`${this.prefix}session.${address}`, JSON.stringify(obj));
    return Promise.resolve();
  }
  public loadSession(address: string): Promise<ArrayBuffer | string> {
    const item = sessionStorage.getItem(`${this.prefix}session.${address}`);
    if (!item) {
      return Promise.resolve(undefined);
    }
    const record = JSON.parse(item);
    if ('string' in record) {
      return Promise.resolve(record.string);
    }
    return Promise.resolve(this.strToBuffer(record.buffer));
  }
  public removeSession(address: string): Promise<void> {
    return Promise.resolve(sessionStorage.removeItem(`${this.prefix}session.${address}`));
  }
  public removeAllSessions(uuid: string): Promise<void> {
    for (let i = 0; i < sessionStorage.length; i++) {
      const key = sessionStorage.key(i);
      if (key.startsWith(`${this.prefix}session.${uuid}`)) {
        sessionStorage.removeItem(key);
        i--;
      }
    }
    return Promise.resolve();
  }
  public hasSession(address: string): boolean {
    return !!sessionStorage.getItem(`${this.prefix}session.${address}`);
  }

  public getLocalRegistrationId(): Promise<number> {
    return Promise.resolve(+sessionStorage.getItem(`${this.prefix}registrationId`));
  }
  public setLocalRegistrationId(id: number) {
    sessionStorage.setItem(`${this.prefix}registrationId`, id + '');
  }
  public getIdentityKeyPair(): Promise<KeyPair> {
    return Promise.resolve(this.getKeyPair(`${this.prefix}identityKey`));
  }
  public setIdentityKeyPair(keypair: KeyPair) {
    this.saveKeyPair(`${this.prefix}identityKey`, keypair);
  }
  public saveIdentity(address: string, identityKey: ArrayBuffer): Promise<boolean> {
    if (address === null || address === undefined) {
      throw new Error('Tried to put identity key for undefined/null key');
    }
    const protoAddress = new libsignal.SignalProtocolAddress.fromString(address);

    const existing = sessionStorage.getItem(`${this.prefix}identityKey.${protoAddress.getName()}`);
    const stringified = this.bufferToStr(identityKey);
    if (stringified.length === 2) {
      console.trace();
    }
    sessionStorage.setItem(`${this.prefix}identityKey.${protoAddress.getName()}`, stringified);

    if (existing && stringified !== existing) {
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }
  public loadIdentityKey(uuid: string): Promise<ArrayBuffer> {
    if (uuid === null || uuid === undefined)
      throw new Error('Tried to get identity key for undefined/null key');
    return Promise.resolve(this.strToBuffer(sessionStorage.getItem(`${this.prefix}identityKey.${uuid}`)));
  }
  public isTrustedIdentity(uuid: string, identityKey: ArrayBuffer): Promise<boolean> {
    if (uuid === null || uuid === undefined) {
      throw new Error('tried to check identity key for undefined/null key');
    }
    if (!(identityKey instanceof ArrayBuffer)) {
      throw new Error('Expected identityKey to be an ArrayBuffer');
    }
    var trusted = sessionStorage.getItem(`${this.prefix}identityKey.${uuid}`);
    if (trusted === null) {
      return Promise.resolve(true);
    }
    return Promise.resolve(this.bufferToStr(identityKey) === trusted);
  }

  public storePreKey(address: string, preKey: KeyPair): Promise<void> {
    this.lastPrekeyId = +address;
    return Promise.resolve(this.saveKeyPair(`${this.prefix}preKey.${address}`, preKey));
  }
  public loadPreKey(address: string): Promise<KeyPair> {
    return Promise.resolve(this.getKeyPair(`${this.prefix}preKey.${address}`));
  }
  public removePreKey(address: string): Promise<void> {
    return Promise.resolve(sessionStorage.removeItem(`${this.prefix}preKey.${address}`));
  }

  public storeSignedPreKey(keyId: number, signedKey: KeyPair): Promise<void> {
    return Promise.resolve(this.saveKeyPair(`${this.prefix}signedKey.${keyId}`, signedKey));
  }
  public loadSignedPreKey(keyId: number): Promise<KeyPair> {
    return Promise.resolve(this.getKeyPair(`${this.prefix}signedKey.${keyId}`));
  }
  public removeSignedPreKey(keyId: number): Promise<void> {
    return Promise.resolve(sessionStorage.removeItem(`${this.prefix}signedKey.${keyId}`));
  }

  public storeMessageKey(roomID: string, key: string, channel: string = '#public') {
    if (!(roomID in this.decryptedKeys)) {
      this.decryptedKeys[roomID] = {};
    }
    if (!(channel in this.decryptedKeys[roomID])) {
      this.decryptedKeys[roomID][channel] = [];
    }
    this.decryptedKeys[roomID][channel].push(key);
    sessionStorage.setItem(`${this.prefix}decryptedKeys`, JSON.stringify(this.decryptedKeys));
  }
  public loadMessageKey(roomID: string, index: number, channel: string = '#public'): string {
    return (
      this.decryptedKeys[roomID] &&
      this.decryptedKeys[roomID][channel] &&
      this.decryptedKeys[roomID][channel][index]
    );
  }

  public storeOwnMessage(roomID: string, message: string, channel: string = '#public') {
    if (!(roomID in this.ownMessages)) {
      this.ownMessages[roomID] = {};
    }
    if (!(channel in this.ownMessages[roomID])) {
      this.ownMessages[roomID][channel] = [];
    }
    this.ownMessages[roomID][channel].push(message);
    sessionStorage.setItem(`${this.prefix}ownMessages`, JSON.stringify(this.ownMessages));
  }
  public loadOwnMessage(roomID: string, index: number, channel: string = '#public'): string {
    return (
      this.ownMessages[roomID] &&
      this.ownMessages[roomID][channel] &&
      this.ownMessages[roomID][channel][index]
    );
  }
}
