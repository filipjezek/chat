import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../store/reducers';
import { submitNick } from '../../shared/actions/auth.actions';

@Component({
  selector: 'chat-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit, AfterViewInit {
  public nickname = new FormControl('', [
    Validators.pattern(/^[^\r\n\t\f\v ()#.]*$/),
    Validators.minLength(3),
    Validators.required,
  ]);

  constructor(private host: ElementRef<HTMLElement>, private store: Store<State>) {}

  ngOnInit() {}

  ngAfterViewInit() {
    const area = window.innerHeight * window.innerWidth;
    if (area > 300000) {
      for (let i = 0; i < area * 0.0002; i++) {
        const el = document.createElement('span');
        el.className = 'pixel';
        el.style.animationDelay = `${Math.random() * 8}s`;
        el.style.top = `${Math.random() * 100}%`;
        el.style.left = `${Math.random() * 100}%`;
        this.host.nativeElement.appendChild(el);
      }
    }
  }

  onSubmit() {
    this.store.dispatch(submitNick({ nick: this.nickname.value }));
  }
}
