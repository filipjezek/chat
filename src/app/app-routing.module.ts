import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { SelectPageComponent } from './select-page/select-page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoggedInGuard } from './auth/logged-in.guard';
import { RoomPageComponent } from './room-page/room-page.component';
import { NotLoggedInGuard } from './auth/not-logged-in.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent,
    pathMatch: 'full',
    canActivate: [NotLoggedInGuard],
    data: { title: 'Babylon | Welcome' }
  },
  {
    path: 'dash',
    component: SelectPageComponent,
    canActivate: [LoggedInGuard],
    data: { title: 'Babylon | Dash' }
  },
  { path: 'room/:id', component: RoomPageComponent, canActivate: [LoggedInGuard] },
  { path: '**', component: NotFoundComponent, data: { title: 'Babylon | 404' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
