export function promiseTimeout(delay: number = 0): Promise<any> {
  return new Promise((r, e) => {
    setTimeout(r, delay);
  });
}
