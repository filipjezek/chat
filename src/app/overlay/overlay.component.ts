import { Component, OnInit, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'chat-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [style({ opacity: 0 }), animate('0.11s ease-out')]),
      transition(':leave', [animate('0.11s ease-in', style({ opacity: 0 }))])
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OverlayComponent implements OnInit {
  @HostBinding('attr.aria-hidden') ariaHidden = true;
  @HostBinding('@fadeInOut') animationProp;

  constructor() {}

  ngOnInit() {}
}
