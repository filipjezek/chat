import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'chat-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() disabled = false;
  @Input() loading = false;
  @Input() submit = true;

  constructor() {}

  ngOnInit() {}
}
