import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'chat-join-room',
  templateUrl: './join-room.component.html',
  styleUrls: ['./join-room.component.scss']
})
export class JoinRoomComponent implements OnInit {
  @Output() join = new EventEmitter<string>();
  form = new FormGroup({
    seed: new FormControl('', [Validators.required, Validators.pattern(/[a-z0-9+-]{8}/i)])
  });

  constructor() {}

  ngOnInit() {}

  onSubmit() {
    this.join.next(this.form.get('seed').value);
  }
}
