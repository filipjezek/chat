import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Unsubscriber } from 'src/app/unsubscriber';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'chat-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.scss'],
})
export class CreateRoomComponent extends Unsubscriber implements OnInit {
  @Output() create = new EventEmitter<string>();

  form = new FormGroup({
    useSeed: new FormControl(''),
    seed: new FormControl({ value: '', disabled: true }, [
      Validators.required,
      Validators.pattern(/^[a-z0-9+-]*$/i),
      Validators.minLength(8),
    ]),
  });

  constructor() {
    super();
  }

  ngOnInit() {
    this.form
      .get('useSeed')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((v) => {
        this.form.get('seed')[v ? 'enable' : 'disable']();
      });
  }

  onSubmit() {
    if (this.form.get('useSeed').value) {
      this.create.next(this.form.get('seed').value);
    } else {
      this.create.next(null);
    }
  }
}
