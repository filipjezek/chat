import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { createRoom, joinRoom, selectPageErrorSeen, leaveRoom } from '../../shared/actions/room.actions';
import { State } from '../store/reducers';
import { Unsubscriber } from '../unsubscriber';
import { takeUntil, filter, take } from 'rxjs/operators';
import { DialogService } from '../services/dialog.service';
import { ToastService } from '../services/toast.service';
import { passphraseMixin } from '../passphrase-mixin';

@Component({
  selector: 'chat-select-page',
  templateUrl: './select-page.component.html',
  styleUrls: ['./select-page.component.scss']
})
export class SelectPageComponent extends passphraseMixin(Unsubscriber) implements OnInit {
  constructor(store: Store<State>, dialogS: DialogService, private toastS: ToastService) {
    super(store, dialogS);
  }

  ngOnInit() {
    super.ngOnInit();

    this.store
      .select(x => x.room.error)
      .pipe(
        takeUntil(this.onDestroy$),
        filter(x => !!x)
      )
      .subscribe(e => {
        this.toastS.add({ level: 'danger', message: e });
        console.error(e);
        this.store.dispatch(selectPageErrorSeen());
      });

    this.store
      .select(x => x.room.currentRoom)
      .pipe(take(1))
      .subscribe(x => {
        if (x) {
          this.store.dispatch(leaveRoom());
        }
      });
  }

  onCreate(id: string) {
    this.store.dispatch(createRoom({ roomID: id }));
  }

  onJoin(id: string) {
    this.store.dispatch(joinRoom({ roomID: id }));
  }
}
