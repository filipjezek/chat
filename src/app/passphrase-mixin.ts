import { Unsubscriber } from './unsubscriber';
import { OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from './store/reducers';
import { takeUntil, distinctUntilChanged, debounceTime, filter } from 'rxjs/operators';
import { DialogService } from './services/dialog.service';
import { cancelJoin, sendPassphrase } from '../shared/actions/room.actions';
import { PassphraseDialogComponent } from './dialogs/passphrase-dialog/passphrase-dialog.component';

export function passphraseMixin(base: typeof Unsubscriber) {
  return class extends base implements OnInit {
    constructor(protected store: Store<State>, protected dialogS: DialogService) {
      super();
    }

    ngOnInit() {
      this.store
        .select(x => x.room.passphraseRequired)
        .pipe(
          takeUntil(this.onDestroy$),
          distinctUntilChanged(),
          debounceTime(150), // because of dialogs closing
          filter(x => !!x)
        )
        .subscribe(() => {
          const ref = this.dialogS.open(PassphraseDialogComponent, false);
          ref.addEventListener('value', (e: CustomEvent) => {
            this.store.dispatch(sendPassphrase({ passphrase: e.detail }));
            this.dialogS.close();
          });
          ref.addEventListener('close', () => {
            this.store.dispatch(cancelJoin());
            this.dialogS.close();
          });
        });
    }
  };
}
