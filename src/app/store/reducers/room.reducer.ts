import { createReducer, on } from '@ngrx/store';
import {
  roomCreated,
  createRoom,
  roomJoined,
  joinRoom,
  passphraseRequired,
  leaveRoom,
  apiError,
  roomPageErrorSeen,
  selectPageErrorSeen,
  roomLocked,
  memberKickedOut,
  memberJoined,
  memberLeft,
  cancelJoin,
  openPrivateConversation,
  closePrivateConversation,
  sendPassphrase,
  messageReceived,
  notificationReceived,
  memberDeleted,
  meKickedOut,
  roomJoinedRawData,
  messageSent,
  whispered,
  whisperReceived
} from '../../../shared/actions/room.actions';
import { colorFromString } from 'src/app/utils/color-from-string';
import { UserStatus } from 'src/shared/interfaces/user-status';
import { Notification, DecryptedMessage } from 'src/shared/interfaces/message';
import { Member } from 'src/shared/interfaces/member';

export interface Room {
  id: string;
  lock: string;
  isAdmin?: boolean;
  members: Member[];
  messages: (DecryptedMessage | Notification)[];
}

export interface State {
  currentRoom: Room;
  joinInProgress: boolean;
  passphraseRequired: boolean;
  nickMask: string;
  whisperingTo?: string;
  error?: any;
}

export const DEFAULT_STATE: State = {
  currentRoom: null,
  joinInProgress: false,
  passphraseRequired: false,
  nickMask: null
};

export const reducer = createReducer<State>(
  DEFAULT_STATE,
  on(roomCreated, roomJoined, (state, action) => {
    const newState = {
      ...state,
      nickMask: action.nick,
      myColor: colorFromString(action.nick)
    };
    if (action.type === roomJoined.type) {
      newState.currentRoom = {
        ...state.currentRoom,
        members: [
          ...action.members
            .filter(m => !state.currentRoom.members.find(curr => curr.nick === m.nick))
            .map(m => ({ ...m, color: colorFromString(m.nick) })),
          ...state.currentRoom.members
        ],
        messages: [...action.messages, ...state.currentRoom.messages],
        lock: state.currentRoom.lock || action.passphrase,
        isAdmin: action.isAdmin
      };
    } else {
      newState.currentRoom = {
        ...state.currentRoom,
        id: action.roomID
      };
      newState.joinInProgress = false;
    }
    return newState;
  }),
  on(createRoom, joinRoom, (state, { roomID, type }) => ({
    ...state,
    joinInProgress: true,
    currentRoom: {
      id: roomID,
      isAdmin: type === createRoom.type,
      lock: null,
      members: [],
      messages: []
    }
  })),
  on(roomJoinedRawData, state => ({
    ...state,
    joinInProgress: false
  })),
  on(passphraseRequired, state => ({
    ...state,
    passphraseRequired: true
  })),
  on(sendPassphrase, state => ({
    ...state,
    passphraseRequired: false
  })),
  on(cancelJoin, state => ({
    ...state,
    joinInProgress: false,
    currentRoom: null,
    passphraseRequired: false
  })),
  on(leaveRoom, () => ({
    ...DEFAULT_STATE
  })),
  on(apiError, (state, { error }) => ({
    ...state,
    error
  })),
  on(roomPageErrorSeen, selectPageErrorSeen, state => ({
    ...state,
    error: null
  })),
  on(roomLocked, (state, { passphrase }) =>
    state.joinInProgress
      ? state
      : {
          ...state,
          currentRoom: { ...state.currentRoom, lock: passphrase }
        }
  ),
  on(memberKickedOut, (state, { member }) =>
    state.joinInProgress
      ? state
      : {
          ...state,
          currentRoom: {
            ...state.currentRoom,
            members: state.currentRoom.members.map(m =>
              m.nick === member ? { ...m, status: UserStatus.KickedOut } : m
            )
          }
        }
  ),
  on(memberJoined, (state, { member }) => {
    if (member === state.nickMask || state.joinInProgress) {
      return state;
    }
    const res = {
      ...state,
      currentRoom: {
        ...state.currentRoom,
        members: [...state.currentRoom.members]
      }
    };
    const reconnected = res.currentRoom.members.findIndex(m => m.nick === member);
    if (reconnected !== -1) {
      res.currentRoom.members[reconnected] = {
        ...res.currentRoom.members[reconnected],
        status: UserStatus.Online
      };
    } else {
      res.currentRoom.members.push({
        nick: member,
        status: UserStatus.Online,
        color: colorFromString(member),
        privateMessages: [],
        unreadMessages: 0
      });
    }
    return res;
  }),
  on(memberLeft, (state, { member }) =>
    state.joinInProgress
      ? state
      : {
          ...state,
          currentRoom: {
            ...state.currentRoom,
            members: state.currentRoom.members.map(m =>
              m.nick === member ? { ...m, status: UserStatus.Offline } : m
            )
          }
        }
  ),
  on(memberDeleted, (state, { member }) =>
    state.joinInProgress
      ? state
      : {
          ...state,
          currentRoom: {
            ...state.currentRoom,
            members: state.currentRoom.members.map(m =>
              m.nick === member ? { ...m, status: UserStatus.Deleted } : m
            )
          }
        }
  ),
  on(leaveRoom, meKickedOut, state => ({
    ...state,
    currentRoom: null
  })),
  on(openPrivateConversation, (state, { member }) => ({
    ...state,
    whisperingTo: member,
    currentRoom: {
      ...state.currentRoom,
      members: state.currentRoom.members.map(m => {
        const newM = { ...m };
        if (m.nick === member) {
          newM.unreadMessages = 0;
        }
        return newM;
      })
    }
  })),
  on(closePrivateConversation, state => ({
    ...state,
    whisperingTo: null
  })),
  on(messageReceived, notificationReceived, messageSent, (state, { message }) =>
    state.joinInProgress
      ? state
      : {
          ...state,
          currentRoom: {
            ...state.currentRoom,
            messages: [...state.currentRoom.messages, message]
          }
        }
  ),
  on(whispered, (state, { message, to }) => ({
    ...state,
    currentRoom: {
      ...state.currentRoom,
      members: state.currentRoom.members.map(m => {
        const newM = { ...m };
        if (m.nick === to) {
          newM.privateMessages = [...m.privateMessages, message];
        }
        return newM;
      })
    }
  })),
  on(whisperReceived, (state, { message }) =>
    state.joinInProgress
      ? state
      : {
          ...state,
          currentRoom: {
            ...state.currentRoom,
            members: state.currentRoom.members.map(m => {
              const newM = { ...m };
              if (m.nick === message.author) {
                newM.privateMessages = [...m.privateMessages, message];
                if (state.whisperingTo !== m.nick) {
                  newM.unreadMessages++;
                }
              }
              return newM;
            })
          }
        }
  )
);
