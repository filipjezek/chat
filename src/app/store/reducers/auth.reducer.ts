import { createReducer, on } from '@ngrx/store';
import {
  sessionCreated,
  submitNick,
  loadSession,
  sessionRestored,
  sessionInvalid
} from '../../../shared/actions/auth.actions';

export interface State {
  user: {
    nick: string;
  };
  authInProgress: boolean;
  error?: any;
}

export const DEFAULT_STATE: State = {
  user: null,
  authInProgress: false
};

export const reducer = createReducer(
  DEFAULT_STATE,
  on(submitNick, (state, { nick }) => ({
    ...state,
    user: {
      nick
    },
    authInProgress: true
  })),
  on(sessionCreated, state => ({
    ...state,
    authInProgress: false
  })),
  on(loadSession, state => ({
    ...state,
    authInProgress: true
  })),
  on(sessionRestored, (state, { nick }) => ({
    ...state,
    user: {
      nick
    },
    authInProgress: false
  })),
  on(sessionInvalid, state => ({
    ...state,
    authInProgress: false
  }))
);
