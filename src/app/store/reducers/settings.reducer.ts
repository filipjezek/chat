import { createReducer, on } from '@ngrx/store';
import { updateSettings, settingsLoaded } from '../actions/settings.actions';

export interface State {
  [room: string]: Settings;
}

export interface Settings {
  blink: boolean;
  tabName: string;
  icon: string;
  scroll: boolean;
  sound: boolean;
}

export const initialState: State = {};

export const defaultSettings: Settings = {
  blink: true,
  tabName: '',
  icon: 'default',
  scroll: true,
  sound: true
};

export const reducer = createReducer(
  initialState,
  on(updateSettings, settingsLoaded, (state, { room, settings }) => {
    const newState = { ...state };
    newState[room] = settings;
    return newState;
  })
);
