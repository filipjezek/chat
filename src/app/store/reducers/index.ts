import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import * as fromAuth from './auth.reducer';
import * as fromRoom from './room.reducer';
import * as fromUI from './ui.reducer';
import * as fromSettings from './settings.reducer';
import { InjectionToken } from '@angular/core';

export interface State {
  auth: fromAuth.State;
  room: fromRoom.State;
  ui: fromUI.State;
  settings: fromSettings.State;
}

const reducers: ActionReducerMap<State> = {
  auth: fromAuth.reducer,
  room: fromRoom.reducer,
  ui: fromUI.reducer,
  settings: fromSettings.reducer
};

export const ROOT_REDUCER = new InjectionToken<ActionReducerMap<State>>('Root Reducer', {
  factory: () => reducers
});

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
