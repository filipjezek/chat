import { createReducer, on } from '@ngrx/store';
import {
  overlayOpen,
  overlayClose,
  changeTitle,
  loadingOverlayIncrement,
  loadingOverlayDecrement
} from '../actions/ui.actions';

export interface State {
  overlay: {
    open: boolean;
    zIndex: number;
    opacity: number;
  };
  title: string;
  icon: string;
  loadingOverlay: number;
}

export const initialState: State = {
  overlay: {
    open: false,
    zIndex: 4,
    opacity: 0.4
  },
  title: 'Babylon',
  icon: 'favicon',
  loadingOverlay: 0
};

export const reducer = createReducer(
  initialState,
  on(overlayOpen, (state, { overlay }) => ({
    ...state,
    overlay: {
      ...state.overlay,
      open: true,
      ...overlay
    }
  })),
  on(overlayClose, state => ({
    ...state,
    overlay: {
      ...initialState.overlay
    }
  })),
  on(changeTitle, (state, { title }) => ({
    ...state,
    title
  })),
  on(loadingOverlayIncrement, state => ({
    ...state,
    loadingOverlay: state.loadingOverlay + 1
  })),
  on(loadingOverlayDecrement, state => ({
    ...state,
    loadingOverlay: state.loadingOverlay - 1
  }))
);
