import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Title } from '@angular/platform-browser';
import { changeTitle, changeIcon } from '../actions/ui.actions';
import { tap } from 'rxjs/operators';
import { IconService } from 'src/app/services/icon.service';

@Injectable()
export class UiEffects {
  changeTitle$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(changeTitle),
        tap(({ title }) => this.title.setTitle(title))
      ),
    { dispatch: false }
  );

  changeIcon$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(changeIcon),
        tap(({ icon }) => this.iconS.setIcon(icon))
      ),
    { dispatch: false }
  );

  constructor(private actions$: Actions, private title: Title, private iconS: IconService) {}
}
