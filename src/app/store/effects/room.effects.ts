import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  exhaustMap,
  tap,
  map,
  take,
  switchMapTo,
  retryWhen,
  takeUntil,
  concatMap,
  withLatestFrom,
} from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  createRoom,
  roomCreated,
  joinRoom,
  roomJoined,
  cancelJoin,
  apiError,
  passphraseRequired,
  sendPassphrase,
  roomJoinedRawData,
  lockRoom,
  leaveRoom,
  kickoutMember,
  whisper,
  sendMessage,
  memberKickedOut,
  meKickedOut,
  whisperReceived,
  messageReceived,
  whisperReceivedEncrypted,
  messageReceivedEncrypted,
  messageSent,
  whispered,
  openPrivateConversation,
} from '../../../shared/actions/room.actions';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { loadingOverlayDecrement, loadingOverlayIncrement } from '../actions/ui.actions';
import { SocketService } from 'src/app/services/socket.service';
import { throwError, race, Observable } from 'rxjs';
import { colorFromString } from 'src/app/utils/color-from-string';
import { EncryptionService } from 'src/app/services/encryption.service';
import { StorageService } from 'src/app/services/storage.service';
import { SocketMessages } from 'src/shared/interfaces/socket-messages';
import { DecryptedMessage } from 'src/shared/interfaces/message';
import { Member } from 'src/shared/interfaces/member';

@Injectable()
export class RoomEffects {
  createRoom$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createRoom),
      tap(() => this.store.dispatch(loadingOverlayIncrement())),
      exhaustMap(({ roomID }) => {
        this.socket.send(SocketMessages.CreateRoom, roomID);
        return this.socket.events.pipe(
          ofType(roomCreated, apiError),
          take(1),
          tap((a) => {
            if (a.type === roomCreated.type) {
              this.router.navigate(['room', a.roomID]);
            }
          })
        );
      }),
      tap(() => this.store.dispatch(loadingOverlayDecrement()))
    )
  );
  joinRoom$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(joinRoom),
        tap(() => this.store.dispatch(loadingOverlayIncrement())),
        exhaustMap(({ roomID }) => {
          this.socket.send(SocketMessages.JoinRoom, roomID);
          return (race(
            this.socket.events.pipe(
              ofType(roomJoinedRawData),
              tap(async (a) => {
                this.store.dispatch(a);

                const read: Record<string, Record<string, number>> = this.storage.getSession(
                  'readMessages',
                  {}
                );
                if (!(roomID in read)) {
                  read[roomID] = {};
                }
                a.members.forEach((m) => {
                  if (!(m.nick in read[roomID])) {
                    read[roomID][m.nick] = 0;
                  }
                });
                this.storage.setSession('readMessages', read);

                this.store.dispatch(
                  roomJoined({
                    members: (await Promise.all(
                      a.members.map(async (m) => ({
                        nick: m.nick,
                        isAdmin: m.isAdmin,
                        status: m.status,
                        color: colorFromString(m.nick),
                        unreadMessages:
                          m.privateMessages.reduce((prev, curr) => {
                            return 'author' in curr ? prev + 1 : prev;
                          }, 0) - read[roomID][m.nick],
                        privateMessages: m.privateMessages.length
                          ? ((await this.encryption
                              .decryptMessages(m.privateMessages, a.nick, m.nick)
                              .toPromise()
                              .catch((e) => console.log('private messages', e))) as DecryptedMessage[])
                          : [],
                      }))
                    ).catch((e) => console.log('members', e))) as any,
                    nick: a.nick,
                    passphrase: a.passphrase,
                    messages: (await this.encryption
                      .decryptMessages(a.messages, a.nick)
                      .toPromise()
                      .catch((e) => console.log('public messages', e))) as any,
                    isAdmin: a.isAdmin,
                  })
                );
                this.router.navigate(['room', roomID]);
              }),
              takeUntil(this.actions$.pipe(ofType(leaveRoom)))
            ),
            this.socket.events.pipe(
              ofType(apiError),
              tap((a) => {
                this.router.navigate(['dash']);
                this.store.dispatch(a);
              }),
              take(1)
            ),
            this.socket.events.pipe(
              ofType(passphraseRequired),
              tap(() => {
                this.store.dispatch(passphraseRequired());
              }),
              switchMapTo(throwError('passphrase'))
            )
          ) as Observable<any>).pipe(
            retryWhen(() => {
              return this.actions$.pipe(
                ofType(sendPassphrase),
                takeUntil(this.actions$.pipe(ofType(cancelJoin))),
                tap<{ passphrase: string }>(({ passphrase }) => {
                  this.socket.send(SocketMessages.JoinRoom, roomID, passphrase);
                })
              );
            })
          );
        }),
        tap(() => this.store.dispatch(loadingOverlayDecrement()))
      ),
    { dispatch: false }
  );
  cancelJoin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(cancelJoin),
      map(() => loadingOverlayDecrement())
    )
  );

  lockRoom$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(lockRoom),
        tap(({ passphrase }) => this.socket.send(SocketMessages.LockRoom, passphrase))
      ),
    { dispatch: false }
  );
  leaveRoom$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(leaveRoom),
        tap(() => this.socket.send(SocketMessages.LeaveRoom))
      ),
    { dispatch: false }
  );
  kickoutMember$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(kickoutMember),
        tap(({ member }) => this.socket.send(SocketMessages.KickoutMember, member))
      ),
    { dispatch: false }
  );
  memberKickedOut$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(memberKickedOut),
        concatMap(({ member }) => this.store.select((x) => x.room.nickMask === member).pipe(take(1))),
        tap((isMe) => {
          if (isMe) {
            this.router.navigateByUrl('/dash');
            setTimeout(() => this.store.dispatch(meKickedOut()));
          }
        })
      ),
    { dispatch: false }
  );
  sendMessage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(sendMessage),
      withLatestFrom(this.store.select((x) => x.room.nickMask)),
      concatMap(([{ message }, nick]) =>
        this.encryption.encryptMessage(message).pipe(
          tap((msg) => {
            this.socket.send(SocketMessages.SendMessage, msg);
          }),
          map((msg) => messageSent({ message: { author: nick, date: msg.date, text: message } }))
        )
      )
    )
  );
  whisper$ = createEffect(() =>
    this.actions$.pipe(
      ofType(whisper),
      withLatestFrom(this.store.select((x) => x.room.nickMask)),
      concatMap(([{ message, to }, nick]) =>
        this.encryption.encryptMessage(message, [to], true).pipe(
          tap((msg) => {
            this.socket.send(SocketMessages.SendMessage, msg);
          }),
          map((msg) => whispered({ message: { author: nick, date: msg.date, text: message }, to }))
        )
      )
    )
  );

  messageReceived$ = createEffect(() =>
    this.actions$.pipe(
      ofType(messageReceivedEncrypted),
      concatMap(({ message }) => this.encryption.decryptMessage(message)),
      map((message) => messageReceived({ message }))
    )
  );
  whisperReceived$ = createEffect(() =>
    this.actions$.pipe(
      ofType(whisperReceivedEncrypted),
      concatMap(({ message }) => this.encryption.decryptMessage(message, undefined, message.author)),
      map((message) => whisperReceived({ message }))
    )
  );

  updateRead$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(whisperReceived, openPrivateConversation),
        concatMap((a) => {
          const m = a.type === whisperReceived.type ? a.message.author : a.member;
          return this.store
            .select((x) => [
              x.room.currentRoom.id,
              x.room.currentRoom.members.find((member) => member.nick === m),
            ])
            .pipe(take(1));
        }),
        tap(([id, m]: [string, Member]) => {
          const read = this.storage.getSession('readMessages');
          const total = m.privateMessages.reduce(
            (prev, curr) => (curr.author === m.nick ? prev + 1 : prev),
            0
          );
          if (read[id][m.nick] !== total - m.unreadMessages) {
            read[id][m.nick] = total - m.unreadMessages;
            this.storage.setSession('readMessages', read);
          }
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<State>,
    private socket: SocketService,
    private encryption: EncryptionService,
    private storage: StorageService
  ) {}
}
