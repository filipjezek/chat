import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap, map } from 'rxjs/operators';
import { loadSettings, settingsLoaded, updateSettings } from '../actions/settings.actions';
import { StorageService } from 'src/app/services/storage.service';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { defaultSettings } from '../reducers/settings.reducer';
import { changeTitle, changeIcon } from '../actions/ui.actions';

@Injectable()
export class SettingsEffects {
  loadSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadSettings),
      map(({ room }) =>
        settingsLoaded({
          settings: this.storage.getSession<State['settings']>('settings', {})[room] || defaultSettings,
          room
        })
      )
    )
  );

  saveSettings$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateSettings),
        tap(({ settings, room }) => {
          const old = this.storage.getSession('settings', {});
          old[room] = settings;
          this.storage.setSession('settings', old);
        })
      ),
    { dispatch: false }
  );

  applySettings$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(settingsLoaded, updateSettings),
        tap(({ settings, room }) => {
          this.store.dispatch(changeIcon({ icon: settings.icon }));
          this.store.dispatch(changeTitle({ title: settings.tabName || room }));
        })
      ),
    { dispatch: false }
  );

  constructor(private actions$: Actions, private storage: StorageService, private store: Store<State>) {}
}
