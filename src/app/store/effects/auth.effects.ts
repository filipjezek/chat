import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  submitNick,
  sessionCreated,
  loadSession,
  sessionInvalid,
  sessionRestored
} from '../../../shared/actions/auth.actions';
import { tap, take, exhaustMap, delay, delayWhen } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SocketService } from 'src/app/services/socket.service';
import { StorageService } from 'src/app/services/storage.service';
import { of, from } from 'rxjs';
import { EncryptionService } from 'src/app/services/encryption.service';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { loadingOverlayIncrement, loadingOverlayDecrement } from '../actions/ui.actions';
import { SocketMessages } from 'src/shared/interfaces/socket-messages';

@Injectable()
export class AuthEffects {
  submitNick$ = createEffect(() =>
    this.actions$.pipe(
      ofType(submitNick),
      tap(() => this.store.dispatch(loadingOverlayIncrement())),
      delayWhen(() => this.actions$.pipe(ofType(loadingOverlayIncrement))),
      delay(30),
      exhaustMap(({ nick }) =>
        from(this.encryption.initSignal()).pipe(
          exhaustMap(bundle => {
            this.socket.send(SocketMessages.NewUser, nick, bundle);
            return this.socket.events.pipe(ofType(sessionCreated), take(1));
          })
        )
      ),
      tap(({ session }) => {
        this.router.navigate(['dash']);
        this.storage.setSession('chat-session', session);
        this.store.dispatch(loadingOverlayDecrement());
      })
    )
  );

  loadSession$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadSession),
      exhaustMap(() => {
        const session = this.storage.getSession('chat-session');
        if (!session) {
          return of(sessionInvalid());
        }
        this.socket.send(SocketMessages.ExistingUser, session);
        return this.socket.events.pipe(ofType(sessionInvalid, sessionRestored), take(1));
      }),
      tap(action => {
        if (action.type === sessionInvalid.type) {
          this.router.navigate(['/']);
          this.storage.removeSession('chat-session');
        }
      })
    )
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private socket: SocketService,
    private storage: StorageService,
    private encryption: EncryptionService,
    private store: Store<State>
  ) {}
}
