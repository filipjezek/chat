import { createAction, props } from '@ngrx/store';
import { Settings } from '../reducers/settings.reducer';

export const updateSettings = createAction(
  '[Settings] Update',
  props<{ room: string; settings: Settings }>()
);
export const loadSettings = createAction('[Settings] Load', props<{ room: string }>());
export const settingsLoaded = createAction(
  '[Settings] Loaded',
  props<{ settings: Settings; room: string }>()
);
