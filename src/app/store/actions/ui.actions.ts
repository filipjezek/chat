import { createAction, props } from '@ngrx/store';

export const overlayOpen = createAction(
  '[Overlay] Open',
  props<{ overlay: { opacity?: number; zIndex?: number } }>()
);
export const overlayClose = createAction('[Overlay] Close');

export const changeTitle = createAction('[Title] change', props<{ title: string }>());
export const changeIcon = createAction('[Icon] change', props<{ icon: string }>());
export const loadingOverlayIncrement = createAction('[Loading Overlay] Increment');
export const loadingOverlayDecrement = createAction('[Loading Overlay] Decrement');
