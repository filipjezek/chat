import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from './store/reducers';
import { map, auditTime, filter, switchMap } from 'rxjs/operators';
import { loadSession } from '../shared/actions/auth.actions';
import { Router, ActivationStart } from '@angular/router';
import { changeTitle } from './store/actions/ui.actions';

@Component({
  selector: 'chat-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  overlayOpen$: Observable<boolean>;
  overlayOpacity$: Observable<number>;
  overlayZIndex$: Observable<number>;
  loadingOverlayOpen$: Observable<boolean>;

  constructor(private store: Store<State>, private router: Router) {
    this.overlayOpen$ = store.select(x => x.ui.overlay.open);
    this.overlayOpacity$ = store.select(x => x.ui.overlay.opacity);
    this.overlayZIndex$ = store.select(x => x.ui.overlay.zIndex);
    this.loadingOverlayOpen$ = store
      .select(x => x.ui.loadingOverlay)
      .pipe(
        map(x => !!x),
        auditTime(5)
      );
  }

  ngOnInit() {
    this.store.dispatch(loadSession());

    this.router.events
      .pipe(
        filter(e => e instanceof ActivationStart),
        switchMap((e: ActivationStart) =>
          e.snapshot.params.id
            ? this.store.select(
                x => (x.settings[e.snapshot.params.id] || ({} as any)).tabName || e.snapshot.params.id
              )
            : of(e.snapshot.data.title)
        )
      )
      .subscribe(title => {
        this.store.dispatch(changeTitle({ title }));
      });
  }
}
