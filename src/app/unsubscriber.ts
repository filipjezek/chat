import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

export abstract class Unsubscriber implements OnDestroy {
  protected onDestroy$ = new Subject<void>();

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
