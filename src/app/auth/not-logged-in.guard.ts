import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../store/reducers';
import { filter, switchMapTo, map, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotLoggedInGuard implements CanActivate {
  constructor(private store: Store<State>, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.store
      .select(x => x.auth.authInProgress)
      .pipe(
        filter(x => !x),
        switchMapTo(this.store.select(x => x.auth.user)),
        map(x => x === null),
        take(1),
        tap(x => {
          if (!x) {
            this.router.navigateByUrl('/dash');
          }
        })
      );
  }
}
