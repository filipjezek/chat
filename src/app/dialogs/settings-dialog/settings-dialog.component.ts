import { Component, OnInit, ElementRef, Input, ChangeDetectionStrategy } from '@angular/core';
import { Dialog } from '../dialog';
import { trigger, state, style, transition, useAnimation } from '@angular/animations';
import { dialogOpen, dialogClose } from 'src/app/animations';
import { FormGroup, FormControl } from '@angular/forms';
import { Settings } from 'src/app/store/reducers/settings.reducer';

@Component({
  selector: 'chat-settings-dialog',
  templateUrl: './settings-dialog.component.html',
  styleUrls: ['./settings-dialog.component.scss'],
  animations: [
    trigger('appear', [
      state(
        'closing',
        style({
          height: 0
        })
      ),
      transition(':enter', [useAnimation(dialogOpen)]),
      transition('* => closing', [useAnimation(dialogClose)])
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsDialogComponent extends Dialog implements OnInit {
  static selector = 'dialog-settings';
  icons = ['default', 'youtube', 'facebook', 'stack-overflow'];

  @Input() set initialValue(settings: Settings) {
    this.form.setValue(settings);
  }

  form = new FormGroup({
    icon: new FormControl('default'),
    tabName: new FormControl(''),
    scroll: new FormControl(true),
    sound: new FormControl(true),
    blink: new FormControl(true)
  });

  constructor(el: ElementRef) {
    super(el);
  }

  ngOnInit() {}

  onSubmit() {
    this.value.next({ ...this.form.value, tabName: this.form.get('tabName').value.trim() });
  }
}
