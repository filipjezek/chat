import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersSelectDialogComponent } from './users-select-dialog.component';

describe('UsersSelectDialogComponent', () => {
  let component: UsersSelectDialogComponent;
  let fixture: ComponentFixture<UsersSelectDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersSelectDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersSelectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
