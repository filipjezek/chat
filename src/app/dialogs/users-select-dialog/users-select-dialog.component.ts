import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Dialog } from '../dialog';
import { trigger, state, style, transition, useAnimation } from '@angular/animations';
import { dialogOpen, dialogClose } from 'src/app/animations';
import { Observable } from 'rxjs';
import { Member } from 'src/shared/interfaces/member';

@Component({
  selector: 'chat-users-select-dialog',
  templateUrl: './users-select-dialog.component.html',
  styleUrls: ['./users-select-dialog.component.scss'],
  animations: [
    trigger('appear', [
      state(
        'closing',
        style({
          height: 0
        })
      ),
      transition(':enter', [useAnimation(dialogOpen)]),
      transition('* => closing', [useAnimation(dialogClose)])
    ])
  ]
})
export class UsersSelectDialogComponent extends Dialog implements OnInit {
  static selector = 'dialog-users-select';

  @Input() users: Observable<Member[]>;
  @Input() type: 'whisper' | 'manage' = 'whisper';

  constructor(el: ElementRef) {
    super(el);
  }

  ngOnInit() {}

  onSelect(nick: string) {
    if (this.type === 'whisper') {
      this.value.next(nick);
    }
  }

  onKickout(nick: string) {
    if (this.type === 'manage') {
      this.value.next(nick);
    }
  }
}
