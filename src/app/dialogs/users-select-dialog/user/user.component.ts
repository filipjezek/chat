import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { UserStatus } from 'src/shared/interfaces/user-status';
import { Member } from 'src/shared/interfaces/member';

@Component({
  selector: 'chat-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnChanges {
  @Input() user: Member;
  @Input() type: 'whisper' | 'manage' = 'whisper';
  @Output() kickout = new EventEmitter<void>();
  @Output() select = new EventEmitter<void>();
  loading = false;
  userStatusEnum = UserStatus;

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (this.loading) {
      if (changes['user'] && changes['user'].currentValue.status === UserStatus.KickedOut) {
        this.loading = false;
      }
    }
  }

  onSelect() {
    this.select.next();
  }

  onKickout(e: MouseEvent) {
    e.stopPropagation();
    if (
      this.loading ||
      this.user.status === UserStatus.KickedOut ||
      this.user.status === UserStatus.Deleted
    ) {
      return;
    }
    this.kickout.next();
    this.loading = true;
  }
}
