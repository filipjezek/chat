import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Dialog } from '../dialog';
import { trigger, state, style, transition, useAnimation } from '@angular/animations';
import { dialogOpen, dialogClose } from 'src/app/animations';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'chat-change-passphrase-dialog',
  templateUrl: './change-passphrase-dialog.component.html',
  styleUrls: ['./change-passphrase-dialog.component.scss'],
  animations: [
    trigger('appear', [
      state(
        'closing',
        style({
          height: 0
        })
      ),
      transition(':enter', [useAnimation(dialogOpen)]),
      transition('* => closing', [useAnimation(dialogClose)])
    ])
  ]
})
export class ChangePassphraseDialogComponent extends Dialog implements OnInit {
  static selector = 'dialog-passphrase-change';

  showPassphrase = false;
  form = new FormGroup({
    passphrase: new FormControl('')
  });

  @Input() lock: string;
  @Input() loading = false;

  constructor(el: ElementRef) {
    super(el);
  }

  ngOnInit() {}

  onSubmit() {
    this.value.next(this.form.get('passphrase').value);
  }
}
