import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePassphraseDialogComponent } from './change-passphrase-dialog.component';

describe('ChangePassphraseDialogComponent', () => {
  let component: ChangePassphraseDialogComponent;
  let fixture: ComponentFixture<ChangePassphraseDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePassphraseDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePassphraseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
