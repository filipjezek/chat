import { Output, EventEmitter, HostBinding, Input, ElementRef, Directive } from '@angular/core';
import { Unsubscriber } from '../unsubscriber';

@Directive({})
export abstract class Dialog extends Unsubscriber {
  @Output() close = new EventEmitter();
  @Output() value = new EventEmitter();
  @HostBinding('@appear') @Input() animationProp;

  constructor(protected el: ElementRef<HTMLElement>) {
    super();
  }

  public focus(): void {
    this.el.nativeElement.setAttribute('tabindex', '-1');
    this.el.nativeElement.focus();
    this.el.nativeElement.setAttribute('tabindex', null);
  }
}
