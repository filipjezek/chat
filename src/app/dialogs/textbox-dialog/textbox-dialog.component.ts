import { Component, OnInit, ElementRef, Input, AfterViewInit } from '@angular/core';
import { Dialog } from '../dialog';
import { trigger, state, style, transition, useAnimation } from '@angular/animations';
import { dialogOpen, dialogClose } from 'src/app/animations';
import { ClipboardService } from 'src/app/services/clipboard.service';

@Component({
  selector: 'chat-textbox-dialog',
  templateUrl: './textbox-dialog.component.html',
  styleUrls: ['./textbox-dialog.component.scss'],
  animations: [
    trigger('appear', [
      state(
        'closing',
        style({
          height: 0
        })
      ),
      transition(':enter', [useAnimation(dialogOpen)]),
      transition('* => closing', [useAnimation(dialogClose)])
    ])
  ]
})
export class TextboxDialogComponent extends Dialog implements OnInit, AfterViewInit {
  static selector = 'dialog-textbox';

  @Input() set link(l: string) {
    this._link = l;
    setTimeout(() => this.selectText());
  }
  get link(): string {
    return this._link;
  }
  private _link: string;
  private inputEl: HTMLInputElement;

  constructor(el: ElementRef, private clipboard: ClipboardService) {
    super(el);
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.inputEl = this.el.nativeElement.querySelector('chat-input input');
  }

  selectText() {
    this.inputEl.select();
    this.inputEl.setSelectionRange(0, this.link.length);
  }

  onCopy() {
    this.selectText();
    this.value.next(this.clipboard.copy());
  }
}
