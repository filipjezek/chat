import { Component, OnInit, Output, EventEmitter, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Dialog } from '../dialog';
import { trigger, transition, style, state, useAnimation } from '@angular/animations';
import { dialogOpen, dialogClose } from 'src/app/animations';

@Component({
  selector: 'chat-passphrase-dialog',
  templateUrl: './passphrase-dialog.component.html',
  styleUrls: ['./passphrase-dialog.component.scss'],
  animations: [
    trigger('appear', [
      state(
        'closing',
        style({
          height: 0
        })
      ),
      transition(':enter', [useAnimation(dialogOpen)]),
      transition('* => closing', [useAnimation(dialogClose)])
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PassphraseDialogComponent extends Dialog implements OnInit {
  static selector = 'dialog-passphrase';

  passphraseForm = new FormGroup({
    passphrase: new FormControl('', Validators.required)
  });

  @Output() value = new EventEmitter();

  constructor(elem: ElementRef) {
    super(elem);
  }

  ngOnInit() {}

  onSubmit() {
    this.value.next(this.passphraseForm.get('passphrase').value);
  }

  onClose() {
    this.close.next();
  }
}
