// address = `${uuid}.${index}`

interface KeyPair {
  pubKey: ArrayBuffer;
  privKey: ArrayBuffer;
}

const libsignal = window['libsignal'];

export class SignalProtocolStore {
  static instanceCount = 0;

  private prefix: string;
  public Direction = {
    SENDING: 1,
    RECEIVING: 2
  };

  constructor() {
    this.prefix = `signal${++SignalProtocolStore.instanceCount}.`;
    this.clear();
  }

  put(key: string, val: string | ArrayBuffer | KeyPair) {
    if (val instanceof ArrayBuffer) {
      sessionStorage.setItem(`${this.prefix}${key}`, this.bufferToStr(val));
    } else if (typeof val === 'object' && 'pubKey' in (val as KeyPair)) {
      this.saveKeyPair(`${this.prefix}${key}`, val as KeyPair);
    } else {
      sessionStorage.setItem(`${this.prefix}${key}`, JSON.stringify(val));
    }
  }
  get(key: string): string | ArrayBuffer | KeyPair {
    const record = JSON.parse(sessionStorage.getItem(`${this.prefix}${key}`));
    if (typeof record === 'string') {
      return record;
    }
    if (typeof record === 'object' && 'pubKey' in record) {
      record.pubKey = new Uint8Array(record.pubKey).buffer;
      record.privKey = new Uint8Array(record.privKey).buffer;
      return record;
    }
    return new Uint8Array(record).buffer;
  }
  remove(key: string) {
    sessionStorage.removeItem(`${this.prefix}${key}`);
  }
  clear() {
    for (let i = 0; i < sessionStorage.length; i++) {
      const key = sessionStorage.key(i);
      if (key.startsWith('signal.')) {
        sessionStorage.removeItem(key);
        i--;
      }
    }
  }

  private bufferToStr(buffer: ArrayBuffer | string): string {
    return window['util'].toString(buffer);
  }
  private strToBuffer(str: string): ArrayBuffer {
    return window['util'].toArrayBuffer(str);
  }
  private saveKeyPair(name: string, keypair: KeyPair) {
    const obj = { pubKey: this.bufferToStr(keypair.pubKey), privKey: this.bufferToStr(keypair.privKey) };
    sessionStorage.setItem(name, JSON.stringify(obj));
  }
  private getKeyPair(name: string): KeyPair {
    const item = sessionStorage.getItem(name);
    if (item) {
      const obj = JSON.parse(item);
      obj.pubKey = this.strToBuffer(obj.pubKey);
      obj.privKey = this.strToBuffer(obj.privKey);
      return obj;
    }
    return undefined;
  }

  public storeSession(address: string, record: ArrayBuffer | string): Promise<void> {
    const obj = {};
    if (record instanceof ArrayBuffer) {
      obj['buffer'] = this.bufferToStr(record);
    } else {
      obj['string'] = record;
    }
    sessionStorage.setItem(`${this.prefix}session.${address}`, JSON.stringify(obj));
    return Promise.resolve();
  }
  public loadSession(address: string): Promise<ArrayBuffer | string> {
    const item = sessionStorage.getItem(`${this.prefix}session.${address}`);
    if (!item) {
      return Promise.resolve(undefined);
    }
    const record = JSON.parse(item);
    if ('string' in record) {
      return Promise.resolve(record.string);
    }
    return Promise.resolve(this.strToBuffer(record.buffer));
  }
  public removeSession(address: string): Promise<void> {
    return Promise.resolve(sessionStorage.removeItem(`${this.prefix}session.${address}`));
  }
  public removeAllSessions(uuid: string): Promise<void> {
    for (let i = 0; i < sessionStorage.length; i++) {
      const key = sessionStorage.key(i);
      if (key.startsWith(`${this.prefix}session.${uuid}`)) {
        sessionStorage.removeItem(key);
        i--;
      }
    }
    return Promise.resolve();
  }

  public getLocalRegistrationId(): Promise<number> {
    return Promise.resolve(+sessionStorage.getItem(`${this.prefix}registrationId`));
  }
  public setLocalRegistrationId(id: number) {
    sessionStorage.setItem(`${this.prefix}registrationId`, id + '');
  }
  public getIdentityKeyPair(): Promise<KeyPair> {
    return Promise.resolve(this.getKeyPair(`${this.prefix}identityKey`));
  }
  public setIdentityKeyPair(keypair: KeyPair) {
    this.saveKeyPair(`${this.prefix}identityKey`, keypair);
  }
  public saveIdentity(address: string, identityKey: ArrayBuffer): Promise<boolean> {
    if (address === null || address === undefined) {
      throw new Error('Tried to put identity key for undefined/null key');
    }
    const protoAddress = new libsignal.SignalProtocolAddress.fromString(address);

    const existing = sessionStorage.getItem(`${this.prefix}identityKey.${protoAddress.getName()}`);
    const stringified = this.bufferToStr(identityKey);
    if (stringified.length === 2) {
      console.trace();
    }
    sessionStorage.setItem(`${this.prefix}identityKey.${protoAddress.getName()}`, stringified);

    if (existing && stringified !== existing) {
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }
  public loadIdentityKey(uuid: string): Promise<ArrayBuffer> {
    if (uuid === null || uuid === undefined)
      throw new Error('Tried to get identity key for undefined/null key');
    return Promise.resolve(this.strToBuffer(sessionStorage.getItem(`${this.prefix}identityKey.${uuid}`)));
  }
  public isTrustedIdentity(uuid: string, identityKey: ArrayBuffer): Promise<boolean> {
    if (uuid === null || uuid === undefined) {
      throw new Error('tried to check identity key for undefined/null key');
    }
    if (!(identityKey instanceof ArrayBuffer)) {
      throw new Error('Expected identityKey to be an ArrayBuffer');
    }
    var trusted = sessionStorage.getItem(`${this.prefix}identityKey.${uuid}`);
    if (trusted === null) {
      return Promise.resolve(true);
    }
    return Promise.resolve(this.bufferToStr(identityKey) === trusted);
  }

  public storePreKey(address: string, preKey: KeyPair): Promise<void> {
    return Promise.resolve(this.saveKeyPair(`${this.prefix}preKey.${address}`, preKey));
  }
  public loadPreKey(address: string): Promise<KeyPair> {
    return Promise.resolve(this.getKeyPair(`${this.prefix}preKey.${address}`));
  }
  public removePreKey(address: string): Promise<void> {
    return Promise.resolve(sessionStorage.removeItem(`${this.prefix}preKey.${address}`));
  }

  public storeSignedPreKey(keyId: number, signedKey: KeyPair): Promise<void> {
    return Promise.resolve(this.saveKeyPair(`${this.prefix}signedKey.${keyId}`, signedKey));
  }
  public loadSignedPreKey(keyId: number): Promise<KeyPair> {
    return Promise.resolve(this.getKeyPair(`${this.prefix}signedKey.${keyId}`));
  }
  public removeSignedPreKey(keyId: number): Promise<void> {
    return Promise.resolve(sessionStorage.removeItem(`${this.prefix}signedKey.${keyId}`));
  }
}
