'use strict';
// address = `${uuid}.${index}`
var libsignal = window['libsignal'];
var SignalProtocolStore = /** @class */ (function() {
  function SignalProtocolStore() {
    this.Direction = {
      SENDING: 1,
      RECEIVING: 2
    };
    this.prefix = 'signal' + ++SignalProtocolStore.instanceCount + '.';
    this.clear();
  }
  SignalProtocolStore.prototype.put = function(key, val) {
    if (val instanceof ArrayBuffer) {
      sessionStorage.setItem('' + this.prefix + key, this.bufferToStr(val));
    } else if (typeof val === 'object' && 'pubKey' in val) {
      this.saveKeyPair('' + this.prefix + key, val);
    } else {
      sessionStorage.setItem('' + this.prefix + key, JSON.stringify(val));
    }
  };
  SignalProtocolStore.prototype.get = function(key) {
    var record = JSON.parse(sessionStorage.getItem('' + this.prefix + key));
    if (typeof record === 'string') {
      return record;
    }
    if (typeof record === 'object' && 'pubKey' in record) {
      record.pubKey = new Uint8Array(record.pubKey).buffer;
      record.privKey = new Uint8Array(record.privKey).buffer;
      return record;
    }
    return new Uint8Array(record).buffer;
  };
  SignalProtocolStore.prototype.remove = function(key) {
    sessionStorage.removeItem('' + this.prefix + key);
  };
  SignalProtocolStore.prototype.clear = function() {
    for (var i = 0; i < sessionStorage.length; i++) {
      var key = sessionStorage.key(i);
      if (key.startsWith('signal.')) {
        sessionStorage.removeItem(key);
        i--;
      }
    }
  };
  SignalProtocolStore.prototype.bufferToStr = function(buffer) {
    return window['util'].toString(buffer);
  };
  SignalProtocolStore.prototype.strToBuffer = function(str) {
    return window['util'].toArrayBuffer(str);
  };
  SignalProtocolStore.prototype.saveKeyPair = function(name, keypair) {
    var obj = { pubKey: this.bufferToStr(keypair.pubKey), privKey: this.bufferToStr(keypair.privKey) };
    sessionStorage.setItem(name, JSON.stringify(obj));
  };
  SignalProtocolStore.prototype.getKeyPair = function(name) {
    var item = sessionStorage.getItem(name);
    if (item) {
      var obj = JSON.parse(item);
      obj.pubKey = this.strToBuffer(obj.pubKey);
      obj.privKey = this.strToBuffer(obj.privKey);
      return obj;
    }
    return undefined;
  };
  SignalProtocolStore.prototype.storeSession = function(address, record) {
    var obj = {};
    if (record instanceof ArrayBuffer) {
      obj['buffer'] = this.bufferToStr(record);
    } else {
      obj['string'] = record;
    }
    sessionStorage.setItem(this.prefix + 'session.' + address, JSON.stringify(obj));
    return Promise.resolve();
  };
  SignalProtocolStore.prototype.loadSession = function(address) {
    var item = sessionStorage.getItem(this.prefix + 'session.' + address);
    if (!item) {
      return Promise.resolve(undefined);
    }
    var record = JSON.parse(item);
    if ('string' in record) {
      return Promise.resolve(record.string);
    }
    return Promise.resolve(this.strToBuffer(record.buffer));
  };
  SignalProtocolStore.prototype.removeSession = function(address) {
    return Promise.resolve(sessionStorage.removeItem(this.prefix + 'session.' + address));
  };
  SignalProtocolStore.prototype.removeAllSessions = function(uuid) {
    for (var i = 0; i < sessionStorage.length; i++) {
      var key = sessionStorage.key(i);
      if (key.startsWith(this.prefix + 'session.' + uuid)) {
        sessionStorage.removeItem(key);
        i--;
      }
    }
    return Promise.resolve();
  };
  SignalProtocolStore.prototype.getLocalRegistrationId = function() {
    return Promise.resolve(+sessionStorage.getItem(this.prefix + 'registrationId'));
  };
  SignalProtocolStore.prototype.setLocalRegistrationId = function(id) {
    sessionStorage.setItem(this.prefix + 'registrationId', id + '');
  };
  SignalProtocolStore.prototype.getIdentityKeyPair = function() {
    return Promise.resolve(this.getKeyPair(this.prefix + 'identityKey'));
  };
  SignalProtocolStore.prototype.setIdentityKeyPair = function(keypair) {
    this.saveKeyPair(this.prefix + 'identityKey', keypair);
  };
  SignalProtocolStore.prototype.saveIdentity = function(address, identityKey) {
    if (address === null || address === undefined) {
      throw new Error('Tried to put identity key for undefined/null key');
    }
    var protoAddress = new libsignal.SignalProtocolAddress.fromString(address);
    var existing = sessionStorage.getItem(this.prefix + 'identityKey.' + protoAddress.getName());
    var stringified = this.bufferToStr(identityKey);
    if (stringified.length === 2) {
      console.trace();
    }
    sessionStorage.setItem(this.prefix + 'identityKey.' + protoAddress.getName(), stringified);
    if (existing && stringified !== existing) {
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  };
  SignalProtocolStore.prototype.loadIdentityKey = function(uuid) {
    if (uuid === null || uuid === undefined)
      throw new Error('Tried to get identity key for undefined/null key');
    return Promise.resolve(this.strToBuffer(sessionStorage.getItem(this.prefix + 'identityKey.' + uuid)));
  };
  SignalProtocolStore.prototype.isTrustedIdentity = function(uuid, identityKey) {
    if (uuid === null || uuid === undefined) {
      throw new Error('tried to check identity key for undefined/null key');
    }
    if (!(identityKey instanceof ArrayBuffer)) {
      throw new Error('Expected identityKey to be an ArrayBuffer');
    }
    var trusted = sessionStorage.getItem(this.prefix + 'identityKey.' + uuid);
    if (trusted === null) {
      return Promise.resolve(true);
    }
    return Promise.resolve(this.bufferToStr(identityKey) === trusted);
  };
  SignalProtocolStore.prototype.storePreKey = function(address, preKey) {
    return Promise.resolve(this.saveKeyPair(this.prefix + 'preKey.' + address, preKey));
  };
  SignalProtocolStore.prototype.loadPreKey = function(address) {
    return Promise.resolve(this.getKeyPair(this.prefix + 'preKey.' + address));
  };
  SignalProtocolStore.prototype.removePreKey = function(address) {
    return Promise.resolve(sessionStorage.removeItem(this.prefix + 'preKey.' + address));
  };
  SignalProtocolStore.prototype.storeSignedPreKey = function(keyId, signedKey) {
    return Promise.resolve(this.saveKeyPair(this.prefix + 'signedKey.' + keyId, signedKey));
  };
  SignalProtocolStore.prototype.loadSignedPreKey = function(keyId) {
    return Promise.resolve(this.getKeyPair(this.prefix + 'signedKey.' + keyId));
  };
  SignalProtocolStore.prototype.removeSignedPreKey = function(keyId) {
    return Promise.resolve(sessionStorage.removeItem(this.prefix + 'signedKey.' + keyId));
  };
  SignalProtocolStore.instanceCount = 0;
  return SignalProtocolStore;
})();
